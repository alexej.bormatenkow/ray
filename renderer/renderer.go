package renderer

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math"
	"math/rand"
	"os"
	cam "ray/camera"
	"ray/lights"
	"ray/objects"
	"ray/scene"
	vec "ray/vector"
	"time"
)

type Renderer struct {
	ResX, ResY int
	Filename   string
	Img        *image.RGBA
	Camera     *cam.Camera
	CameraV2   *cam.CameraV2
	Scene      *scene.Scene
}

func NewRenderer(resX int, resY int, filename string, cam *cam.Camera, cam2 *cam.CameraV2, scene *scene.Scene) *Renderer {
	// Fill the image with white pixels
	img := image.NewRGBA(image.Rect(0, 0, resX, resY))
	white := color.RGBA{255, 255, 255, 255}
	for y := 0; y < resY; y++ {
		for x := 0; x < resX; x++ {
			img.Set(x, y, white)
		}
	}

	return &Renderer{
		ResX:     resX,
		ResY:     resY,
		Filename: filename,
		Img:      img,
		Camera:   cam,
		CameraV2: cam2,
		Scene:    scene,
	}
}

func (r *Renderer) Info() {
	fmt.Println("cam r: ", r.Camera.R)
	fmt.Println("cam u: ", r.Camera.U)
	fmt.Println("cam position: ", r.Camera.Position)
	fmt.Println("cam direction: ", r.Camera.Direction)
}

func (r *Renderer) GammaOut(color vec.Vec3) vec.Vec3 {
	return vec.Vec3{X: math.Sqrt(color.X), Y: math.Sqrt(color.Y), Z: math.Sqrt(color.Z)}
}

func (r *Renderer) D(alpha float64, N vec.Vec3, H vec.Vec3) float64 {
	numerator := math.Pow(alpha, 2)

	//NH := math.Max(N.Dot(H), 0.)

	NH := N.Dot(H)
	denominator := math.Pi * math.Pow((math.Pow(NH, 2)*(math.Pow(alpha, 2)-1)+1), 2)
	//denominator = math.Max(denominator, 0.01)
	return numerator / denominator
}

func (r *Renderer) G(alpha float64, N vec.Vec3, H vec.Vec3) float64 {

	numerator := math.Max(N.Dot(H), 0.)
	k := alpha / 2.

	// denominator := math.Max(N.Dot(H), 0.)*(1.-k) + k
	denominator := N.Dot(H)*(1.-k) + k

	denominator = math.Max(denominator, 0.01)
	return numerator / denominator
}

func (r *Renderer) F(F0 float64, V vec.Vec3, H vec.Vec3) float64 {
	return F0 + (1.-F0)*math.Pow(1.-math.Max(V.Dot(H), 0.), 5)
}

func (r *Renderer) GenerateRay(x, y float64) *cam.Ray {
	// convert pixel coordinates to normalized device coordinates between -1 and 1
	ndcX := (float64(x)+0.5)/float64(r.ResX)*2.0 - 1.0
	ndcY := (float64(y)+0.5)/float64(r.ResY)*2.0 - 1.0

	// calculate the point on the image plane in world space
	aspectRatio := float64(r.ResX) / float64(r.ResY)
	fov := r.Camera.FOV * math.Pi / 180.0
	halfHeight := math.Tan(fov / 2.0)
	halfWidth := aspectRatio * halfHeight
	a := r.Camera.U.ScalarMult(ndcX * halfWidth)
	b := r.Camera.R.ScalarMult(ndcY * halfHeight)
	imgPlanePoint := r.Camera.Position.Add(a).Add(b).Add(r.Camera.Direction)

	// calculate the direction vector in world space
	dir := imgPlanePoint.Sub(r.Camera.Position) //.Norm()

	// swap x and y coordinates of the direction vector
	//dir = vec.Vec3{X: dir.Y, Y: dir.X, Z: dir.Z}

	// create the ray
	ray := cam.NewRay(r.Camera.Position, dir)
	return ray
}

func (r *Renderer) Trace(ray *cam.Ray, depth int) vec.Vec3 {
	if depth <= 0 {
		return vec.Vec3{X: 0., Y: 0., Z: 0.} // return black
	}

	var finalColor vec.Vec3 = vec.Vec3{X: 0., Y: 0., Z: 0.}
	var reflectionColor vec.Vec3 = vec.Vec3{X: 0., Y: 0., Z: 0.}
	var refractionColor vec.Vec3 = vec.Vec3{X: 0., Y: 0., Z: 0.}

	// iterate through objects in scene and check for the nearest intersection
	intersected, nearestIntersection, nearestObject, nearestNormal := r.intersectScene(ray)
	if intersected {
		// compute reflections and refractions
		reflectivity := nearestObject.GetMaterial().Reflectivity
		transmission := nearestObject.GetMaterial().Transmission
		position := ray.Position(nearestIntersection)
		if reflectivity > 0. {
			NV := nearestNormal.Dot(ray.Direction) * 2.
			N2 := nearestNormal.ScalarMult(NV)
			reflectionDirection := ray.Direction.Sub(N2)
			reflectionRay := cam.NewRay(position, reflectionDirection)
			reflectionColor = r.Trace(reflectionRay, depth-1)
			reflectionColor = reflectionColor.ScalarMult(reflectivity)
		}
		if transmission > 0. {
			i := nearestObject.GetMaterial().Transmission / 1.
			a := ray.Direction.Neg().Dot(nearestNormal)
			a2 := a * a
			i2 := i * i
			b := math.Sqrt(1. - (i2 * (1. - a2)))

			transmissionDirection := ray.Direction.ScalarMult(i).Add(nearestNormal.ScalarMult(i*a - b))

			refractionRay := cam.NewRay(position, transmissionDirection)
			refractionRay.Origin = refractionRay.Origin.Sub(nearestNormal.ScalarMult(0.0001))

			refractionColor = r.Trace(refractionRay, depth-1)
			//refractionColor = refractionColor.ScalarMult(0.2)
			//return refractionColor
		}

		// test shadows
		shadowRay := cam.NewRay(position, r.Scene.Lights[0].GetPosition().Sub(position).Norm())
		shadowRay.Origin = shadowRay.Origin.Add(nearestNormal.ScalarMult(0.0001))
		shadowIntersected, _, _, _ := r.intersectScene(shadowRay)

		// if not in shadow compute color
		if !shadowIntersected {
			finalColor = r.computeColor(nearestObject.GetMaterial(), nearestIntersection, position, nearestNormal, r.Scene.Lights[0])
			// refactor color mixing and use coefficients to regulate each part of the material props

			// add reflection
			finalColor = finalColor.Add(reflectionColor)
			//fmt.Println("finalColor: ", finalColor)
			finalColor = finalColor.Add(refractionColor)

		} else {
			finalColor = r.computeColor(nearestObject.GetMaterial(), nearestIntersection, position, nearestNormal, r.Scene.Lights[0])
			// add reflection
			//finalColor = finalColor.Add(reflectionColor)
			//fmt.Println("finalColor: ", finalColor)
			//finalColor = finalColor.Add(refractionColor)

			samples := 50
			light := 0.
			shadow := 0.

			for i := 0; i < samples; i++ {
				shadowRay := cam.NewRay(position, r.Scene.Lights[0].GetRandomPosition().Sub(position).Norm())
				shadowRay.Origin = shadowRay.Origin.Add(shadowRay.Direction.ScalarMult(0.00001))
				shadowIntersected, distance, _, _ := r.intersectScene(shadowRay)
				if !shadowIntersected {
					light++
				} else {
					shadow += 1. / distance
					//fmt.Println(1. / distance)
				}
			}
			// average the results
			//avg := float64(light) / float64(samples)
			avgShadow := shadow / float64(samples)
			//fmt.Println("average: ", avg)
			shadowColor := vec.Vec3{X: 1. - avgShadow, Y: 1. - avgShadow, Z: 1. - avgShadow}
			finalColor = finalColor.Mult(shadowColor)
			//finalColor = shadowColor
			//finalColor = finalColor.ScalarMult(1. - avg)
			finalColor = finalColor.Add(reflectionColor)
			//fmt.Println("finalColor: ", finalColor)
			finalColor = finalColor.Add(refractionColor)
		}
	} else {
		//finalColor = r.Scene.Background
		finalColor = r.Scene.IntersectSky(*ray)
		//fmt.Println(*&ray.Direction)
	}

	return finalColor
}

func (r *Renderer) intersectScene(ray *cam.Ray) (bool, float64, objects.Object, vec.Vec3) {
	var nearestObject objects.Object
	var nearestIntersection float64
	var nearestNormal vec.Vec3
	for _, object := range r.Scene.Objects {
		intersected, intersection, _, normal := object.Intersect(*ray)
		if intersected {
			if nearestObject == nil || intersection < nearestIntersection {
				nearestObject = object
				nearestIntersection = intersection
				nearestNormal = normal
			}
		}
	}
	return nearestObject != nil, nearestIntersection, nearestObject, nearestNormal
}

func (r *Renderer) computeColor(material objects.Material, intersection float64, position vec.Vec3, objNormal vec.Vec3, light lights.Light) vec.Vec3 {
	// "our" cook torrance model D * F * G / (4 * (N dot L) * (N dot V))
	N := objNormal.Norm()                       // normal at intersection point
	V := r.Camera.Position.Sub(position).Norm() // vector from intersection point to camera aka intersection of ray negated
	L := light.GetPosition().Sub(position).Norm()

	// H is the half vector between V and L – if its the same as N, the surface is perfectly smooth
	H := L.Add(V).Norm()

	// D is a statistical distribution of microfacets
	alpha := material.Roughness
	D := r.D(alpha, N, H)

	// F is the fresnel term -> how much light is reflected at the surface in relation to the angle of incidence
	F := r.F(0.04, V, H)

	// G is the geometric attenuation term -> how much microfacets are shading each other
	G := r.G(alpha, N, H)

	NL := math.Max(N.Dot(L), 0.)
	NV := math.Max(N.Dot(V), 0.)

	cookTorranceNumerator := D * F * G

	cookTorranceDenominator := 4. * NL * NV
	cookTorranceDenominator = math.Max(cookTorranceDenominator, 1.)

	k_s := cookTorranceNumerator / cookTorranceDenominator

	//fmt.Println("k_s: ", k_s)

	k_d := 1. - k_s*(1-material.Metalness)

	lightIntensity := light.GetDistanceIntensity(position)
	lightColor := light.GetColor()

	PBR := material.Color.ScalarMult(k_d)
	PBR.X += k_s
	PBR.Y += k_s
	PBR.Z += k_s
	finalColor := lightIntensity.ScalarMult(NL).Mult(lightColor).Mult(PBR)

	return finalColor
}

func (r *Renderer) Clamp(val float64) float64 {
	if val > 255 {
		return 255.
	} else if val < 0 {
		return 0.
	} else {
		return val
	}
}

func (r *Renderer) meanColor(samples []vec.Vec3) vec.Vec3 {
	var averageColor vec.Vec3 = vec.Vec3{X: 0., Y: 0., Z: 0.}
	var size float64 = float64(len(samples))
	for _, sample := range samples {
		averageColor = averageColor.Add(sample)
	}
	averageColor = averageColor.ScalarMult(1. / size)
	return averageColor
}

func (r *Renderer) varianceColor(samples []vec.Vec3, mean vec.Vec3) float64 {
	var variance float64 = 0.
	var size float64 = float64(len(samples))

	for _, sample := range samples {
		diff := sample.Sub(mean)
		variance += math.Pow(diff.X, 2) + math.Pow(diff.Y, 2) + math.Pow(diff.Z, 2)
	}
	variance = variance / size
	return variance
}

func (r *Renderer) SuperSample(x float64, y float64, width float64, height float64, ssDepth int, recDepth int) vec.Vec3 {
	//var variance float64 = 0.
	var samples = []vec.Vec3{}

	for i := 0.; i <= width; i++ {
		for j := 0.; j <= height; j++ {
			topLeft := r.GenerateRay(x, y)
			topRight := r.GenerateRay(x+i, y)
			bottomLeft := r.GenerateRay(x, y+j)
			bottomRight := r.GenerateRay(x+i, y+j)

			//fmt.Println("topLeft: ", r.Trace(topLeft, recDepth))
			//fmt.Println("topRight: ", topRight)
			//fmt.Println("bottomLeft: ", bottomLeft)
			//fmt.Println("bottomRight: ", bottomRight)

			samples = append(samples, r.Trace(topLeft, recDepth))
			samples = append(samples, r.Trace(topRight, recDepth))
			samples = append(samples, r.Trace(bottomLeft, recDepth))
			samples = append(samples, r.Trace(bottomRight, recDepth))
			//fmt.Println(samples)
		}
	}
	// calculate average color
	var averageColor = r.meanColor(samples)
	//fmt.Println("averageColor: ", averageColor)
	// calculate variance
	variance := r.varianceColor(samples, averageColor)

	if variance > 0.01 && ssDepth > 0 {
		samples = nil
		//fmt.Println("depth: ", ssDepth, " variance: ", variance)
		halfWidth := width / 2.
		halfHeight := height / 2.

		topLeft := r.SuperSample(x, y, halfWidth, halfHeight, ssDepth-1, recDepth)
		topRight := r.SuperSample(x+halfWidth, y, halfWidth, halfHeight, ssDepth-1, recDepth)
		bottomLeft := r.SuperSample(x, y+halfHeight, halfWidth, halfHeight, ssDepth-1, recDepth)
		bottomRight := r.SuperSample(x+halfWidth, y+halfHeight, halfWidth, halfHeight, ssDepth-1, recDepth)

		samples = append(samples, topLeft)
		samples = append(samples, topRight)
		samples = append(samples, bottomLeft)
		samples = append(samples, bottomRight)

		return r.meanColor(samples)

	}
	return r.meanColor(samples)
}

// Peter Shirley's random sampler
func (r *Renderer) RandomSampler(u, v float64, recDepth, samples int) vec.Vec3 {
	colorVector := vec.Vec3{X: 0., Y: 0., Z: 0.}
	for i := 0; i < samples; i++ {
		randU := u + rand.Float64()/float64(r.ResX)
		randV := v + rand.Float64()/float64(r.ResY)
		ray := r.CameraV2.GenerateRay(randU, randV)
		colorVector = colorVector.Add(r.Trace(ray, recDepth))
	}

	colorVector = colorVector.ScalarMult(1. / float64(samples))
	return colorVector
}

func (r *Renderer) Render(recDepth int, ss bool, ssDepth int) {
	start := time.Now()

	for y := 0; y < r.ResY; y++ {
		for x := 0; x < r.ResX; x++ {
			color := color.RGBA{}
			var colorVector vec.Vec3
			// normalize coordinates for ray!
			u := float64(x) / float64(r.ResX)
			v := float64(y) / float64(r.ResY)

			// random sampling
			if ss && ssDepth > 0 {
				// colorVector = r.SuperSample(float64(x), float64(y), 2., 2., ssDepth, recDepth)
				colorVector = r.RandomSampler(u, v, recDepth, ssDepth)
			} else {
				// Generate ray for pixel (x,y)
				//ray := r.GenerateRay(float64(x), float64(y))

				//fmt.Println("u: ", u, " v: ", v)
				ray := r.CameraV2.GenerateRay(u, v)
				// fmt.Println("ray: ", ray.Direction)
				// trace single ray per pixel
				colorVector = r.Trace(ray, recDepth)
			}

			albedo := colorVector.ScalarMult(255.)
			color.R = uint8(r.Clamp(albedo.X))
			color.G = uint8(r.Clamp(albedo.Y))
			color.B = uint8(r.Clamp(albedo.Z))
			color.A = 255

			r.Img.Set(x, y, color)
		}
	}

	elapsed := time.Since(start) // calculate the elapsed time
	seconds := elapsed.Seconds()
	fmt.Printf("Finished rendering in %v seconds\n", seconds)

	// write the image to a PNG file
	f, _ := os.Create(r.Filename + ".png")
	defer f.Close()
	png.Encode(f, r.Img)
}
