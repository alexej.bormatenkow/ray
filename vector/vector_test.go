package vector

import (
	"math"
	"testing"
)

func TestAdd(t *testing.T) {
	a := Vec3{X: .5, Y: 1.0, Z: -2.0}
	b := Vec3{X: .5, Y: -1.0, Z: 4.0}
	c := Vec3{X: 1.0, Y: .0, Z: 2.0}

	if a.Add(b) != c {
		t.Errorf("got %v, wanted %v", a.Add(b), c)
	}
}

func TestSub(t *testing.T) {
	a := Vec3{X: .5, Y: 1.0, Z: -2.0}
	b := Vec3{X: .5, Y: -1.0, Z: 4.0}
	c := Vec3{X: .0, Y: 2.0, Z: -6.0}

	if a.Sub(b) != c {
		t.Errorf("got %v, wanted %v", a.Sub(b), c)
	}
}

func TestScalarMult(t *testing.T) {
	a := Vec3{X: .5, Y: 1.0, Z: -2.0}
	s := .4

	as := Vec3{X: .2, Y: .4, Z: -.8}

	if a.ScalarMult(s) != as {
		t.Errorf("got %v, wanted %v", a.ScalarMult(s), as)
	}

	b := Vec3{X: .5, Y: -1.0, Z: 4.0}
	s1 := -2.2

	bs1 := Vec3{X: -1.1, Y: 2.2, Z: -8.8}

	if b.ScalarMult(s1) != bs1 {
		t.Errorf("got %v, wanted %v", b.ScalarMult(s1), bs1)
	}

}

func TestScalarDiv(t *testing.T) {
	a := Vec3{X: .5, Y: 1.0, Z: -2.0}
	s := .4

	as := Vec3{X: 1.25, Y: 2.5, Z: -5}

	if a.ScalarDiv(s) != as {
		t.Errorf("got %v, wanted %v", a.ScalarDiv(s), as)
	}

	b := Vec3{X: .5, Y: -1.0, Z: 4.0}
	s1 := -2.2

	bs1 := Vec3{X: -0.22727272727272727, Y: 0.45454545454545453, Z: -1.8181818181818181}

	if b.ScalarDiv(s1) != bs1 {
		t.Errorf("got %v, wanted %v", b.ScalarDiv(s1), bs1)
	}
}

func TestDot(t *testing.T) {
	// Test case 1: Basic test with small integers
	a := Vec3{1, 2, 3}
	b := Vec3{4, 5, 6}
	expected := 32.0
	result := a.Dot(b)
	if result != expected {
		t.Errorf("Dot product of %v and %v was incorrect, got: %v, want: %v.", a, b, result, expected)
	}

	// Test case 2: Test with negative components
	c := Vec3{-2, 3, 1}
	d := Vec3{4, -5, 2}
	expected = -21.0
	result = c.Dot(d)
	if result != expected {
		t.Errorf("Dot product of %v and %v was incorrect, got: %v, want: %v.", c, d, result, expected)
	}

	// Test case 3: Test with large integers
	e := Vec3{100000, 200000, 300000}
	f := Vec3{400000, 500000, 600000}
	expected = 320000000000.0
	result = e.Dot(f)
	if result != expected {
		t.Errorf("Dot product of %v and %v was incorrect, got: %v, want: %v.", e, f, result, expected)
	}

	// Test case 4: Test with non-integer values
	g := Vec3{1.5, 2.5, 3.5}
	h := Vec3{4.5, 5.5, 6.5}
	expected = 43.25
	result = g.Dot(h)
	if result != expected {
		t.Errorf("Dot product of %v and %v was incorrect, got: %v, want: %v.", g, h, result, expected)
	}

	// Test case 5: Test with one vector of zero magnitude
	i := Vec3{0, 0, 0}
	j := Vec3{4, 5, 6}
	expected = 0.0
	result = i.Dot(j)
	if result != expected {
		t.Errorf("Dot product of %v and %v was incorrect, got: %v, want: %v.", i, j, result, expected)
	}

	// Test case 6: Test with both vectors of zero magnitude
	k := Vec3{0, 0, 0}
	l := Vec3{0, 0, 0}
	expected = 0.0
	result = k.Dot(l)
	if result != expected {
		t.Errorf("Dot product of %v and %v was incorrect, got: %v, want: %v.", k, l, result, expected)
	}
}

func TestCross(t *testing.T) {
	// Test case with two non-parallel vectors
	a := Vec3{1, 2, 3}
	b := Vec3{4, 5, 6}
	expected := Vec3{-3, 6, -3}
	result := a.Cross(b)
	if result != expected {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with two parallel vectors
	a = Vec3{1, 2, 3}
	b = Vec3{2, 4, 6}
	expected = Vec3{0, 0, 0}
	result = a.Cross(b)
	if result != expected {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with one vector that is the zero vector
	a = Vec3{1, 2, 3}
	b = Vec3{0, 0, 0}
	expected = Vec3{0, 0, 0}
	result = a.Cross(b)
	if result != expected {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with negative input vectors
	a = Vec3{1, -2, 3}
	b = Vec3{-4, 5, -6}
	expected = Vec3{-3, -6, -3}
	result = a.Cross(b)
	if result != expected {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with input vectors that are not orthogonal
	a = Vec3{1, 2, 0}
	b = Vec3{0, 0, 1}
	expected = Vec3{2, -1, 0}
	result = a.Cross(b)
	if result != expected {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
}

func TestLength(t *testing.T) {
	// Test case with non-zero vector
	a := Vec3{3, 4, 5}
	expected := 7.0710678118654755
	result := a.Length()
	if math.Abs(result-expected) > 0.0001 {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with zero vector
	a = Vec3{0, 0, 0}
	expected = 0
	result = a.Length()
	if math.Abs(result-expected) > 0.0001 {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with negative input vector
	a = Vec3{-3, 4, -5}
	expected = 7.0710678118654755
	result = a.Length()
	if math.Abs(result-expected) > 0.0001 {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
}

func TestNorm(t *testing.T) {
	// Test case with non-zero vector
	a := Vec3{3, 4, 5}
	expected := Vec3{0.4242640687119285, 0.565685424949238, 0.7071067811865475}
	result := a.Norm()
	if math.Abs(result.X-expected.X) > 0.0001 || math.Abs(result.Y-expected.Y) > 0.0001 || math.Abs(result.Z-expected.Z) > 0.0001 {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with zero vector
	a = Vec3{0, 0, 0}
	expected = Vec3{0, 0, 0}
	result = a.Norm()
	if result != expected {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case with negative input vector
	a = Vec3{-3, 4, -5}
	expected = Vec3{-0.4242640687119285, 0.565685424949238, -0.7071067811865475}
	result = a.Norm()
	if math.Abs(result.X-expected.X) > 0.0001 || math.Abs(result.Y-expected.Y) > 0.0001 || math.Abs(result.Z-expected.Z) > 0.0001 {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
}
