package vector

import (
	"math"
)

type Vec3 struct {
	X, Y, Z float64
}

type Vec4 struct { // Grundrechenarten für 4 dimensionale Vektoren implementieren?
	X, Y, Z, W float64
}

func (a Vec3) Add(b Vec3) Vec3 {
	a.X = a.X + b.X
	a.Y = a.Y + b.Y
	a.Z = a.Z + b.Z
	return a
}

func (a Vec3) Sub(b Vec3) Vec3 {
	a.X = a.X - b.X
	a.Y = a.Y - b.Y
	a.Z = a.Z - b.Z
	return a
}

func (a Vec3) Mult(b Vec3) Vec3 {
	a.X = a.X * b.X
	a.Y = a.Y * b.Y
	a.Z = a.Z * b.Z
	return a
}

func (a Vec3) Neg() Vec3 {
	a.X = -a.X
	a.Y = -a.Y
	a.Z = -a.Z
	return a
}

func (a Vec3) DistanceTo(b Vec3) float64 {
	x := a.X - b.X
	y := a.Y - b.Y
	z := a.Z - b.Z
	dist := math.Sqrt(x*x + y*y + z*z)
	return dist
}

func (a Vec3) ScalarMult(b float64) Vec3 {
	a.X = a.X * b
	a.Y = a.Y * b
	a.Z = a.Z * b
	return a
}

func (a Vec3) ScalarDiv(b float64) Vec3 {
	a.X = a.X / b
	a.Y = a.Y / b
	a.Z = a.Z / b
	return a
}

func (a Vec3) Dot(b Vec3) float64 {
	prod := a.X*b.X + a.Y*b.Y + a.Z*b.Z
	return prod
}

func (a Vec3) Cross(b Vec3) Vec3 {
	x := a.Y*b.Z - a.Z*b.Y
	y := a.Z*b.X - a.X*b.Z
	z := a.X*b.Y - a.Y*b.X
	return Vec3{
		X: x,
		Y: y,
		Z: z,
	}
}

func (a Vec3) Length() float64 {
	length := math.Sqrt(a.Dot(a))
	return length
}

func (a Vec3) Norm() Vec3 {
	length := a.Length()
	if length == 0 {
		return Vec3{0, 0, 0}
	}
	test := 1.0 / length
	norm := a.ScalarMult(test)
	return norm
}
