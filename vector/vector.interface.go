package vector

type Vector interface {
	Add(Vec3) Vec3
	Sub(Vec3) Vec3
	Mult(Vec3) Vec3
	Neg() Vec3
	DistanceTo(Vec3) float64
	ScalarMult(float64) Vec3
	ScalarDiv(float64) Vec3
	Dot(Vec3) float64
	Cross(Vec3) Vec3
	Length() float64
	Norm() Vec3
}
