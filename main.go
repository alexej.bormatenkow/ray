package main

import (
	cam "ray/camera"
	lights "ray/lights"
	obj "ray/objects"
	render "ray/renderer"
	scene "ray/scene"
	vec "ray/vector"
)

func main() {

	// camera
	camera := cam.NewCamera(vec.Vec3{X: .4, Y: -0.5, Z: 30}, vec.Vec3{X: .0, Y: .0, Z: -100.0}, vec.Vec3{X: .0, Y: 1.0, Z: 0.0}, -.3, 500, 500)
	// camera v2
	cameraV2 := cam.NewCameraV2(vec.Vec3{X: 1.4, Y: -2.5, Z: 35}, vec.Vec3{X: .0, Y: .0, Z: -1.0}, vec.Vec3{X: .0, Y: 1.0, Z: 0.0}, 30, 1920./1080., .8)

	// light

	// light1 := lights.NewSun(vec.Vec3{X: .0, Y: .0, Z: 22.}, vec.Vec3{X: .405, Y: .405, Z: .405})

	light1 := lights.NewArtificial(vec.Vec3{X: 5.0, Y: -7.0, Z: 16.}, vec.Vec3{X: .9, Y: .9, Z: .9})

	lightCollection := []lights.Light{light1}

	sphere01 := obj.NewSphere(vec.Vec3{X: 1.0, Y: 1.0, Z: 1.}, 3., vec.Vec3{X: 1., Y: 1., Z: .0})
	sphere02 := obj.NewSphere(vec.Vec3{X: 3., Y: 2., Z: 5.3}, .9, vec.Vec3{X: 1.0, Y: 1., Z: 1.0})
	sphere01.Material.Reflectivity = .0
	sphere01.Material.Transmission = .0

	sphere02.Material.Reflectivity = .2
	sphere02.Material.Transmission = 0.3

	sphere03 := obj.NewSphere(vec.Vec3{X: 1., Y: 70., Z: 1.}, 65.7, vec.Vec3{X: .9, Y: .9, Z: .9})
	sphere03.Material.Reflectivity = .2

	sphere04 := obj.NewSphere(vec.Vec3{X: -5., Y: 1., Z: -7.2}, 3., vec.Vec3{X: .0, Y: 1., Z: .0})

	// create quadric sphere
	/* sphere03 := obj.NewQuadSphere(2, vec.Vec3{X: 0.9, Y: 0.85, Z: .95})
	sphere04 := obj.NewQuadSphere(2, vec.Vec3{X: 0.9, Y: 0.85, Z: .95})

	sphere03.Shift(vec.Vec3{X: .0, Y: .0, Z: 3.5})
	sphere04.Shift(vec.Vec3{X: -.5, Y: -1.5, Z: 3.0})

	difference00 := obj.NewDifference(sphere03.Clone(), sphere04.Clone(), vec.Vec3{X: 0.9, Y: 0.85, Z: .95}) */

	// create yellow material
	materialPurple := obj.Material{ // default material
		Color:        vec.Vec3{X: 1., Y: 1.0, Z: .0},
		Roughness:    .1,
		Metalness:    .9,
		Diffuse:      .9,
		Reflectivity: .4,
	}

	sphere01.AddMaterial(materialPurple)

	objects := []obj.Object{sphere01}
	objects = append(objects, sphere02)
	objects = append(objects, sphere03)
	objects = append(objects, sphere04)

	/* step := 1.
	nameCounter := 0
	for i := 0.; i < 360; i += step {

		// Calculate light position and target vectors
		angle := float64(i) * math.Pi / 180.0
		radius := 15.0
		lightY := radius * math.Sin(angle)
		lightZ := radius * math.Cos(angle)
		lightPos := vec.Vec3{X: 4., Y: lightY, Z: lightZ}

		// Create light with new position and target
		light := lights.NewArtificial(lightPos, vec.Vec3{X: 1.0, Y: 1.0, Z: 1.0})

		lightCollection := []lights.Light{light}

		// create a test scene
		myscene := scene.NewScene(objects, lightCollection)

		// create renderer
		name := "render" + fmt.Sprintf("%v", nameCounter)
		renderer := render.NewRenderer(1280, 720, name, camera, myscene)

		nameCounter++

		//renderer.Info()
		renderer.Trace()

	} */

	// ffmpeg -framerate 24 -i render%d.png -c:v libx264 -pix_fmt yuv420p outputDiffuse.mp4

	// some objects
	/* sphere01 := obj.NewSphere(vec.Vec3{X: .0, Y: 0., Z: 5.5}, 1., vec.Vec3{X: 0.9, Y: 0.85, Z: .95})
	sphere02 := obj.NewSphere(vec.Vec3{X: .0, Y: .0, Z: 2.5}, 1., vec.Vec3{X: 0.0, Y: 1.0, Z: 0.0})

	difference := obj.NewDifference(sphere02.Clone(), sphere01.Clone(), vec.Vec3{X: 0., Y: .9, Z: .0})

	objects := []obj.Object{difference} */

	// create a test scene
	myscene := scene.NewScene(objects, lightCollection, vec.Vec3{X: 1.0, Y: .0, Z: .0}) // background color black

	myscene.AddSky("skybox_test.png")
	// create renderer

	renderer := render.NewRenderer(1920, 1080, "render", camera, cameraV2, myscene)

	//renderer.Info()
	renderer.Render(3, true, 40)

}
