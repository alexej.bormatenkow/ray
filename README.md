## ray

a ray tracer, written in go. part of a semester assignment at htw berlin, summer 2023

### Features
- PBR / Cook-Torrance Reflection Model
- quadric shapes
- constructive Solid Geometry (CSG) with basic support for boolean operations
- experimental super sampling
- thin lens camera with defocus blur
- skybox support

![Test Rendering](render.png)

---
### Sources:

 [**“Physically Based Rendering: From Theory To Implementation”**](https://pbr-book.org/)   by M. Pharr, W. Jakob, G. Humphreys


**“The Ray Tracer Challenge – A Test-Driven Guide to Your First 3D Renderer”**,
J. Buck, 1st Ed. 2019, The Pragmatic Programmers

[**“Ray Tracing in One Weekend - 12 Defocus Blur”**](https://raytracing.github.io/books/RayTracingInOneWeekend.html#defocusblur), 
P. Shirley

[**“Wikipedia – Cube Mapping”**](https://en.wikipedia.org/wiki/Cube_mapping) 
