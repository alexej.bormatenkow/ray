package scene

import (
	"fmt"
	"image"
	"image/draw"
	"math"
	"os"
	cam "ray/camera"
	"ray/lights"
	"ray/objects"
	vec "ray/vector"
)

type SkyBox struct {
	Full   image.Image
	Left   image.Image
	Right  image.Image
	Top    image.Image
	Bottom image.Image
	Front  image.Image
	Back   image.Image
	resX   int
	resY   int
}

type Scene struct {
	Objects    []objects.Object
	Lights     []lights.Light
	Background vec.Vec3
	Sky        SkyBox
	SkyPresent bool
}

type SceneInterface interface {
	AddObject(*objects.Object)
	AddLight(*lights.Light)
	ShowLights()
	ShowObjects()
}

func NewScene(objects []objects.Object, lights []lights.Light, background vec.Vec3) *Scene {
	return &Scene{
		Objects:    objects,
		Lights:     lights,
		Background: background,
	}
}

func (s *Scene) AddObject(object *objects.Object) {
	s.Objects = append(s.Objects, *object)
}

func (s *Scene) AddLight(light *lights.Light) {
	s.Lights = append(s.Lights, *light)
}

func (s *Scene) ShowObjects() {
	for index, object := range s.Objects {
		fmt.Printf("Index: %d, Object: %v\n", index, object)
	}
}

func (s *Scene) AddSky(filepath string) {
	// Read image from file that already exists
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	pngImg, _, err := image.Decode(file)
	if err != nil {
		fmt.Println(err)
	}

	// loading faces
	front, err := os.Open("front.png")
	if err != nil {
		fmt.Println(err)
	}
	defer front.Close()

	pngFront, _, err := image.Decode(front)
	if err != nil {
		fmt.Println(err)
	}
	s.Sky.Front = pngFront

	back, err := os.Open("back.png")
	if err != nil {
		fmt.Println(err)
	}
	defer back.Close()

	pngBack, _, err := image.Decode(back)
	if err != nil {
		fmt.Println(err)
	}
	s.Sky.Back = pngBack

	left, err := os.Open("left.png")
	if err != nil {
		fmt.Println(err)
	}
	defer left.Close()

	pngLeft, _, err := image.Decode(left)
	if err != nil {
		fmt.Println(err)
	}
	s.Sky.Left = pngLeft

	right, err := os.Open("right.png")
	if err != nil {
		fmt.Println(err)
	}
	defer right.Close()

	pngRight, _, err := image.Decode(right)
	if err != nil {
		fmt.Println(err)
	}
	s.Sky.Right = pngRight

	top, err := os.Open("top.png")
	if err != nil {
		fmt.Println(err)
	}
	defer top.Close()

	pngTop, _, err := image.Decode(top)
	if err != nil {
		fmt.Println(err)
	}
	s.Sky.Top = pngTop

	bottom, err := os.Open("bottom.png")
	if err != nil {
		fmt.Println(err)
	}
	defer bottom.Close()

	pngBottom, _, err := image.Decode(bottom)
	if err != nil {
		fmt.Println(err)
	}
	s.Sky.Bottom = pngBottom

	// convert to rgba
	rgbaImg := image.NewRGBA(pngImg.Bounds())
	draw.Draw(rgbaImg, rgbaImg.Bounds(), pngImg, image.Point{}, draw.Src)

	// full texture, just to be sure
	s.Sky.Full = rgbaImg
	faceWidth := rgbaImg.Bounds().Max.X / 4
	faceHeight := rgbaImg.Bounds().Max.Y / 3
	s.Sky.resX = faceWidth
	s.Sky.resY = faceHeight

	// split into faces
	//s.Sky.Left = rgbaImg.SubImage(image.Rect(0, faceHeight, faceWidth, 2*faceHeight))
	//s.Sky.Right = rgbaImg.SubImage(image.Rect(2*faceWidth, faceHeight, 3*faceWidth, 2*faceHeight))
	//s.Sky.Top = rgbaImg.SubImage(image.Rect(faceWidth, 0, 2*faceWidth, faceHeight))
	//s.Sky.Bottom = rgbaImg.SubImage(image.Rect(faceWidth, 2*faceHeight, 2*faceWidth, 3*faceHeight))
	//s.Sky.Front = rgbaImg.SubImage(image.Rect(faceWidth, faceHeight, 2*faceWidth, 2*faceHeight))
	//s.Sky.Back = rgbaImg.SubImage(image.Rect(3*faceWidth, faceHeight, 4*faceWidth, 2*faceHeight))

	// show me the resolution of the faces
	/* fmt.Println("left:", s.Sky.Left.Bounds().Max.X, s.Sky.Left.Bounds().Max.Y)
	fmt.Println("right:", s.Sky.Right.Bounds().Max.X, s.Sky.Right.Bounds().Max.Y)
	fmt.Println("top:", s.Sky.Top.Bounds().Max.X, s.Sky.Top.Bounds().Max.Y)
	fmt.Println("bottom:", s.Sky.Bottom.Bounds().Max.X, s.Sky.Bottom.Bounds().Max.Y)
	fmt.Println("front:", s.Sky.Front.Bounds().Max.X, s.Sky.Front.Bounds().Max.Y)
	fmt.Println("back:", s.Sky.Back.Bounds().Max.X, s.Sky.Back.Bounds().Max.Y) */

	// export faces as png
	/* leftFile, err := os.Create("left.png")
	if err != nil {
		// handle error
	}
	defer leftFile.Close()
	if err := png.Encode(leftFile, s.Sky.Left); err != nil {
		// handle error
	}
	rightFile, err := os.Create("right.png")
	if err != nil {
		// handle error
	}
	defer rightFile.Close()
	if err := png.Encode(rightFile, s.Sky.Right); err != nil {
		// handle error
	}

	topFile, err := os.Create("top.png")
	if err != nil {
		// handle error
	}
	defer topFile.Close()
	if err := png.Encode(topFile, s.Sky.Top); err != nil {
		// handle error
	}

	bottomFile, err := os.Create("bottom.png")
	if err != nil {
		// handle error
	}
	defer bottomFile.Close()
	if err := png.Encode(bottomFile, s.Sky.Bottom); err != nil {
		// handle error
	}

	frontFile, err := os.Create("front.png")
	if err != nil {
		// handle error
	}
	defer frontFile.Close()
	if err := png.Encode(frontFile, s.Sky.Front); err != nil {
		// handle error
	}

	backFile, err := os.Create("back.png")
	if err != nil {
		// handle error
	}
	defer backFile.Close()
	if err := png.Encode(backFile, s.Sky.Back); err != nil {
		// handle error
	} */
	s.SkyPresent = true
	fmt.Println("skybox successfully loaded")
}

func (s *Scene) IntersectSky(ray cam.Ray) vec.Vec3 {
	// based on https://en.wikipedia.org/wiki/Cube_mapping

	dir := ray.Direction

	// get the absolute values of the direction vector
	absX := math.Abs(dir.X)
	absY := math.Abs(dir.Y)
	absZ := math.Abs(dir.Z)

	// which direction is negative/positive?
	isXPositive := dir.X > 0
	isYPositive := dir.Y > 0
	isZPositive := dir.Z > 0

	var max, uc, vc float64
	var face image.Image
	//var faceName string

	// positive x
	if isXPositive && absX >= absY && absX >= absZ {
		// u (0 to 1) goes from +z to -z
		// v (0 to 1) goes from -y to +y
		max = absX
		vc = -dir.Z
		uc = dir.Y
		// load corresponding face
		//fmt.Println("positive x")
		face = s.Sky.Right
		//faceName = "left"
		//return vec.Vec3{X: 1.0, Y: 0.0, Z: 0.0} // red
	}
	// negative x
	if !isXPositive && absX >= absY && absX >= absZ {
		// u (0 to 1) goes from -z to +z
		// v (0 to 1) goes from -y to +y
		max = absX
		vc = dir.Z
		uc = dir.Y
		// load corresponding face
		//fmt.Println("negative x")
		face = s.Sky.Left
		//faceName = "right"
		//return vec.Vec3{X: 0.0, Y: 1.0, Z: 0.0} // green
	}
	// positive y
	if isYPositive && absY >= absX && absY >= absZ {
		// u (0 to 1) goes from -x to +x
		// v (0 to 1) goes from +z to -z
		max = absY
		vc = dir.X
		uc = -dir.Z
		// load corresponding face
		//fmt.Println("positive y")
		face = s.Sky.Bottom
		//faceName = "bottom"
		//return vec.Vec3{X: 0.0, Y: 0.0, Z: 1.0} // blue
	}
	// negative y
	if !isYPositive && absY >= absX && absY >= absZ {
		// u (0 to 1) goes from -x to +x
		// v (0 to 1) goes from -z to +z
		max = absY
		vc = dir.X
		uc = dir.Z
		// load corresponding face
		//fmt.Println("negative y")
		face = s.Sky.Top
		//faceName = "top"
		//return vec.Vec3{X: 1.0, Y: 1.0, Z: 0.0} // yellow
	}
	// positive z
	if isZPositive && absZ >= absX && absZ >= absY {
		// u (0 to 1) goes from -x to +x
		// v (0 to 1) goes from -y to +y
		max = absZ
		vc = dir.X
		uc = dir.Y
		// load corresponding face
		//fmt.Println("positive z")
		face = s.Sky.Back
		//faceName = "front"
		//return vec.Vec3{X: 0.0, Y: 1.0, Z: 1.0} // cyan

	}
	// negative z
	if !isZPositive && absZ >= absX && absZ >= absY {
		// u (0 to 1) goes from +x to -x
		// v (0 to 1) goes from -y to +y
		// return pixel color from corresponding face
		max = absZ
		vc = -dir.X
		uc = dir.Y
		// load corresponding face
		//fmt.Println("negative z")
		face = s.Sky.Front

		//return vec.Vec3{X: 1.0, Y: 0.0, Z: 1.0} // magenta
	}
	u := 0.5 * (uc/max + 1)
	v := 0.5 * (vc/max + 1)
	//fmt.Println("u: ", u, "v: ", v)

	//fmt.Println("u: ", u, "v: ", v)

	// get pixel color from corresponding face
	y := int(u * float64(face.Bounds().Dx()))
	x := int(v * float64(face.Bounds().Dy()))
	//fmt.Println("x: ", x, "y: ", y)
	//fmt.Println(face.Bounds().Max.X, face.Bounds().Max.Y)
	pixel := face.At(x, y)

	r, g, b, _ := pixel.RGBA()
	/* if r == 0 && g == 0 && b == 0 {
		//fmt.Println("u: ", u, "v: ", v)
		//fmt.Println("x: ", x, "y: ", y)
		fmt.Println("face: ", faceName)
	} */
	return vec.Vec3{X: float64(r) / 65535, Y: float64(g) / 65535, Z: float64(b) / 65535}
}

func (s *Scene) ShowLights() {
	for index, light := range s.Lights {
		fmt.Printf("Index: %d, Light: %v\n", index, light)
	}
}
