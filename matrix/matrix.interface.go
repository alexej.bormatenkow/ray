package matrix

type Matrix interface {
	Get(row int, col int) float64
	Set(row int, col int, val float64)
	Rows() int
	Cols() int
	//transpose() Mat
}
