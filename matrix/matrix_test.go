package matrix

import (
	"reflect"
	"testing"
)

func TestNewMatrix(t *testing.T) {
	// Test case with a valid dimension
	m1, err1 := NewMatrix(3)
	if err1 != nil {
		t.Errorf("NewMatrix(3) returned an error: %v", err1)
	}
	if m1.Rows() != 3 {
		t.Errorf("Expected 3 rows, got %d", m1.Rows())
	}
	if m1.Cols() != 0 {
		t.Errorf("Expected 0 columns, got %d", m1.Cols())
	}

	// Test case with a dimension less than 2
	m2, err2 := NewMatrix(1)
	if err2 == nil {
		t.Errorf("Expected an error, got nil")
	}
	if m2 != nil {
		t.Errorf("Expected a nil matrix, got %v", m2)
	}

	// Test case with a dimension greater than 4
	m3, err3 := NewMatrix(5)
	if err3 == nil {
		t.Errorf("Expected an error, got nil")
	}
	if m3 != nil {
		t.Errorf("Expected a nil matrix, got %v", m3)
	}
}

func TestIdentity(t *testing.T) {
	// Test case with a valid dimension
	m1, err1 := Identity(3)
	if err1 != nil {
		t.Errorf("Identity(3) returned an error: %v", err1)
	}
	if m1.Rows() != 3 {
		t.Errorf("Expected 3 rows, got %d", m1.Rows())
	}
	if m1.Cols() != 3 {
		t.Errorf("Expected 3 columns, got %d", m1.Cols())
	}
	if m1.Get(0, 0) != 1.0 || m1.Get(1, 1) != 1.0 || m1.Get(2, 2) != 1.0 {
		t.Errorf("Expected identity matrix, got %v", m1)
	}

	// Test case with a dimension less than 2
	m2, err2 := Identity(1)
	if err2 == nil {
		t.Errorf("Expected an error, got nil")
	}
	if !reflect.DeepEqual(m2, Mat{}) {
		t.Errorf("Expected a zero-valued matrix, got %v", m2)
	}

	// Test case with a dimension greater than 4
	m3, err3 := Identity(5)
	if err3 == nil {
		t.Errorf("Expected an error, got nil")
	}
	if !reflect.DeepEqual(m3, Mat{}) {
		t.Errorf("Expected a zero-valued matrix, got %v", m3)
	}
}
