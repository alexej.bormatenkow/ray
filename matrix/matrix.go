package matrix

import (
	"errors"
)

type Mat struct {
	Vals [][]float64
}

// returns a matrix filled with zeros
func NewMatrix(dim int) (*Mat, error) {
	if dim < 2 || dim > 4 {
		return nil, errors.New("matrix dimensions must be between 2 and 4")
	} else {
		vals := make([][]float64, dim)
		for i := 0; i < dim; i++ {
			vals[i] = make([]float64, dim)
			for j := 0; j < dim; j++ {
				vals[i][j] = 0.0
			}
		}
		return &Mat{
			Vals: vals,
		}, nil
	}
}

func Identity(dim int) (Mat, error) {
	if dim < 2 || dim > 4 {
		return Mat{}, errors.New("matrix dimensions must be between 2 and 4")
	} else {
		// Create a new matrix with the given dimensions
		matrix := Mat{
			Vals: make([][]float64, dim),
		}

		// Initialize each inner slice with the required length
		for i := range matrix.Vals {
			matrix.Vals[i] = make([]float64, dim)
		}

		// Set the diagonal elements to 1.0 (the identity elements)
		for i := 0; i < dim; i++ {
			matrix.Vals[i][i] = 1.0
		}

		return matrix, nil
	}
}

func (m Mat) Rows() int {
	return len(m.Vals)
}

func (m Mat) Cols() int {
	return len(m.Vals[0])
}

func (m Mat) Get(row int, col int) float64 {
	return m.Vals[row][col]
}

func (m Mat) Set(row int, col int, val float64) {
	m.Vals[row][col] = val
}

// no dedicated vector multiplication, because a vector is nothing else than a 1*X / X*1 matrix
func (m Mat) Mult(n Mat) (Mat, error) {
	if m.Cols() != n.Rows() {
		return Mat{}, errors.New("matrix dimensions do not match")
	}

	res := Mat{Vals: make([][]float64, m.Rows())}
	for i := range res.Vals {
		res.Vals[i] = make([]float64, n.Cols())
	}

	for i := 0; i < m.Rows(); i++ {
		for j := 0; j < n.Cols(); j++ {
			sum := 0.0
			for k := 0; k < m.Cols(); k++ {
				sum += m.Vals[i][k] * n.Vals[k][j]
			}
			res.Vals[i][j] = sum
		}
	}

	return res, nil
}

func (m Mat) Transpose() Mat {
	rows, cols := m.Cols(), m.Rows()
	transposed := Mat{Vals: make([][]float64, rows)}

	for i := range transposed.Vals {
		transposed.Vals[i] = make([]float64, cols)
	}

	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			transposed.Vals[i][j] = m.Vals[j][i]
		}
	}

	return transposed
}

func (m Mat) Inverse() (Mat, error) {
	if m.Rows() != m.Cols() {
		return Mat{}, errors.New("matrix must be square")
	}

	n := m.Rows()
	augmented := Mat{Vals: make([][]float64, n)}

	for i := range augmented.Vals {
		augmented.Vals[i] = make([]float64, 2*n)
		copy(augmented.Vals[i], m.Vals[i])
		augmented.Vals[i][n+i] = 1
	}

	for i := 0; i < n; i++ {
		pivot := augmented.Vals[i][i]
		if pivot == 0 {
			return Mat{}, errors.New("matrix is singular")
		}

		for j := 0; j < 2*n; j++ {
			augmented.Vals[i][j] /= pivot
		}

		for j := 0; j < n; j++ {
			if i == j {
				continue
			}

			factor := augmented.Vals[j][i]
			for k := 0; k < 2*n; k++ {
				augmented.Vals[j][k] -= factor * augmented.Vals[i][k]
			}
		}
	}

	inverse := Mat{Vals: make([][]float64, n)}
	for i := range inverse.Vals {
		inverse.Vals[i] = augmented.Vals[i][n:]
	}

	return inverse, nil
}
