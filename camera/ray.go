package camera

import (
	"fmt"
	vec "ray/vector"
)

type Ray struct {
	Origin, Direction vec.Vec3
}

func NewRay(origin vec.Vec3, direction vec.Vec3) *Ray {
	return &Ray{
		Origin:    origin,
		Direction: direction,
	}
}

type RayInterface interface {
	position(float64) vec.Vec3
	// method for intersection counts with pointer to object and number of intersection with that object
}

func (r Ray) Position(s float64) vec.Vec3 {
	return r.Origin.Add(r.Direction.ScalarMult(s))
}

func (r Ray) Info() {
	fmt.Printf("--- \n at direction: %v, origin: %v \n ---\n", r.Direction, r.Origin)
}
