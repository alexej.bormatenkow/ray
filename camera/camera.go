package camera

import (
	"math"
	vec "ray/vector"
)

type Camera struct {
	Position         vec.Vec3 // Kameraposition (ein Punkt im Raum)
	Focus            vec.Vec3 // Punkt auf den die Kamera gerichtet ist
	Direction        vec.Vec3 // Blickrichtung (ein Vektor)
	U                vec.Vec3 // Vektor der in der Ebene "oben" festlegt
	R                vec.Vec3 // Vektor der in der Ebene "rechts" festlegt
	ImagePlaneDist   float64  // Distance from the camera position to the image plane
	ImagePlaneWidth  float64  // Width of the image plane
	ImagePlaneHeight float64  // Height of the image plane
	FOV              float64  // Field of view
}

func NewCamera(position, focus, u vec.Vec3, imagePlaneDist, imagePlaneWidth, imagePlaneHeight float64) *Camera {

	direction := focus.Sub(position)

	// Berechne r als das Kreuzprodukt von u und v, negiert
	r := u.Cross(direction).Neg()

	// Normalisiere die Vektoren u, v und r
	u = u.Norm()
	direction = direction.Norm()
	r = r.Norm()

	// Erstelle die Kamera mit den berechneten Vektoren
	return &Camera{
		Position:         position,
		Focus:            focus,
		Direction:        direction,
		U:                u,
		R:                r,
		ImagePlaneDist:   imagePlaneDist,
		ImagePlaneWidth:  imagePlaneWidth,
		ImagePlaneHeight: imagePlaneHeight,
		FOV:              2.0 * math.Atan((imagePlaneWidth/2.0)/imagePlaneDist),
	}
}
