package camera

import (
	"fmt"
	"math"
	"math/rand"
	vec "ray/vector"
)

// programmed after Peter Shirley's camera model https://raytracing.github.io/books/RayTracingInOneWeekend.html#defocusblur

type CameraV2 struct {
	position        vec.Vec3 // origin
	direction       vec.Vec3
	up              vec.Vec3
	horizontal      vec.Vec3
	vertical        vec.Vec3
	lowerLeftCorner vec.Vec3
	vfov            float64
	aspectRatio     float64
	theta           float64
	h               float64
	u               vec.Vec3
	v               vec.Vec3
	viewportHeight  float64
	viewportWidth   float64
	lensRadius      float64
	focusDist       float64
}

func NewCameraV2(position, direction, up vec.Vec3, vfov, aspectRatio, aperture float64) *CameraV2 {

	// viewport with fov distortion taken into account
	theta := vfov * math.Pi / 180                 // vertical field of view in radians
	h := math.Tan(theta / 2)                      // half of the height of my viewport
	viewportHeight := 2.0 * h                     // viewport height based on theta
	viewportWidth := aspectRatio * viewportHeight // viewport width pased on provided aspect ratio

	// coordinate system for camera
	w := position.Sub(direction).Norm() // distance from origin to direction, normalized
	u := up.Cross(w).Norm()             // u coordinate "the real up"
	v := w.Cross(u)                     // v coordinate "horizontal axis"

	// focal distance is calculated like w: length between origin and direction, to determine the length
	focusDist := position.Sub(direction).Length()
	horizontal := u.ScalarMult(viewportWidth * focusDist)
	vertical := v.ScalarMult(viewportHeight * focusDist)

	w = w.ScalarMult(focusDist) // we use the focal distance to scale our camera direction
	lowerLeftCorner := position.Sub(horizontal.ScalarMult(0.5)).Sub(vertical.ScalarMult(0.5)).Sub(w)

	lensRadius := aperture / 2
	fmt.Println("lens radius: ", lensRadius)

	return &CameraV2{
		vfov:            vfov,
		aspectRatio:     aspectRatio,
		position:        position,
		direction:       direction,
		up:              up,
		viewportHeight:  viewportHeight,
		viewportWidth:   viewportWidth,
		horizontal:      horizontal,
		vertical:        vertical,
		theta:           theta,
		h:               h,
		u:               u,
		v:               v,
		lowerLeftCorner: lowerLeftCorner,
		lensRadius:      lensRadius,
		focusDist:       focusDist,
	}
}

func (c *CameraV2) GenerateRay(s, t float64) *Ray { // takes in the normalized pixel coordinates of the viewport
	random := c.randomUnitDisk().ScalarMult(c.lensRadius)
	offset := c.u.ScalarMult(random.X).Add(c.v.ScalarMult(random.Y))
	origin := c.position.Add(offset)

	direction := c.lowerLeftCorner.Add(c.horizontal.ScalarMult(s)).Add(c.vertical.ScalarMult(t)).Sub(c.position).Sub(offset)
	return NewRay(origin, direction)
}

func (c *CameraV2) randomUnitDisk() vec.Vec3 {
	var p vec.Vec3
	for {
		x := rand.Float64()*2 - 1
		y := rand.Float64()*2 - 1
		p = vec.Vec3{X: x, Y: y, Z: 0.0}
		if p.Dot(p) >= 1.0 {
			continue
		}
		return p
	}

}
