package lights

import (
	"math"
	rand "math/rand"
	vec "ray/vector"
)

type Artificial struct {
	Name      string
	Position  vec.Vec3
	Direction vec.Vec3
	Intensity vec.Vec3
	Color     vec.Vec3
}

func NewArtificial(position vec.Vec3, intensity vec.Vec3) *Artificial {
	return &Artificial{
		Name:      "Artificial",
		Position:  position,
		Intensity: intensity,
		Color:     vec.Vec3{X: 1., Y: 1., Z: 1.},
	}
}

func (a *Artificial) SetPosition(position vec.Vec3) {
	a.Position = position
}

func (a *Artificial) SetIntensity(intensity vec.Vec3) {
	a.Intensity = intensity
}

func (a *Artificial) GetPosition() vec.Vec3 {
	return a.Position
}

func (a *Artificial) GetRandomPosition() vec.Vec3 {
	// Generate random spherical coordinates
	theta := rand.Float64() * 4 * math.Pi
	phi := math.Acos(2*rand.Float64() - 1)

	// Convert spherical coordinates to Cartesian coordinates
	radius := 0.9 // Adjust the radius as needed
	x := radius * math.Sin(phi) * math.Cos(theta)
	y := radius * math.Sin(phi) * math.Sin(theta)
	z := radius * math.Cos(phi)

	// Create a random position vector
	randomPosition := vec.Vec3{X: x, Y: y, Z: z}

	// Offset the random position from the actual position
	randomPosition = randomPosition.Add(a.Position)

	return randomPosition
	/* // returns a random position around the actual position with normal distribution
	randomPositionX := a.clamp(rand.NormFloat64(), .9)
	randomPositionY := a.clamp(rand.NormFloat64(), .9)
	randomPositionZ := a.clamp(rand.NormFloat64(), .9)
	randomPosition := vec.Vec3{X: randomPositionX, Y: randomPositionY, Z: randomPositionZ}
	//randomPosition = randomPosition.Norm()
	randomPosition = randomPosition.Add(a.Position)
	//randomPosition = a.Position
	return randomPosition */
}

func (a *Artificial) clamp(val, rad float64) float64 {
	return math.Min(rad, math.Max(-rad, val))
}

func (a *Artificial) GetIntensity() vec.Vec3 {
	return a.Intensity
}

/* func (a *Artificial) GetDirection() vec.Vec3 {
	return a.Direction
}

func (a *Artificial) SetDirection(direction vec.Vec3) {
	a.Direction = direction
} */

func (a *Artificial) GetDistanceIntensity(point vec.Vec3) vec.Vec3 {
	return a.Intensity.ScalarDiv(a.Position.DistanceTo(point)*(a.Position.DistanceTo(point)) + 1).Norm()
}

func (a *Artificial) GetIntensityAt(point vec.Vec3) vec.Vec3 {
	return a.Intensity
}

func (a *Artificial) GetColor() vec.Vec3 {
	return a.Color
}
