package lights

import (
	rand "math/rand"
	vec "ray/vector"
)

type Sun struct {
	Name      string
	Position  vec.Vec3
	Intensity vec.Vec3
}

func NewSun(position vec.Vec3, intensity vec.Vec3) *Sun {
	return &Sun{
		Name:      "Sun",
		Position:  position,
		Intensity: intensity,
	}
}

func (s *Sun) SetPosition(position vec.Vec3) {
	s.Position = position
}

func (s *Sun) SetIntensity(intensity vec.Vec3) {
	s.Intensity = intensity
}

func (s *Sun) GetPosition() vec.Vec3 {
	return s.Position

}

func (s *Sun) GetRandomPosition() vec.Vec3 {
	// returns a random position around the actual position with normal distribution
	randomPositionX := rand.NormFloat64()*0.5 + s.Position.X
	randomPositionY := rand.NormFloat64()*0.5 + s.Position.Y
	randomPositionZ := rand.NormFloat64()*0.5 + s.Position.Z
	randomPosition := vec.Vec3{X: randomPositionX, Y: randomPositionY, Z: randomPositionZ}

	return randomPosition
}

func (s *Sun) GetIntensity() vec.Vec3 {
	return s.Intensity
}

func (s *Sun) GetDistanceIntensity(point vec.Vec3) vec.Vec3 { // sun has no distance falloff, rewrite...
	return s.Intensity.ScalarDiv(s.Position.DistanceTo(point)*(s.Position.DistanceTo(point)) + 1)
}
