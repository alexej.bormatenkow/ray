package lights

import (
	vec "ray/vector"
)

type Light interface {
	GetPosition() vec.Vec3
	GetRandomPosition() vec.Vec3
	GetIntensity() vec.Vec3
	SetPosition(vec.Vec3)
	SetIntensity(vec.Vec3)
	GetDistanceIntensity(vec.Vec3) vec.Vec3
	GetIntensityAt(vec.Vec3) vec.Vec3
	GetColor() vec.Vec3
}
