package objects

import (
	"fmt"
	"math"
	ray "ray/camera"
	"ray/matrix"
	vec "ray/vector"
)

type QuadSphere struct {
	Quad     Quadric
	Color    vec.Vec3
	Material Material
}

func NewQuadSphere(radius float64, color vec.Vec3) *QuadSphere {
	material := Material{ // default material
		Color:     color,
		Roughness: .1,
		Metalness: .9,
		Diffuse:   .9,
	}

	cylQuad := NewQuadric(1., 1., 1., 0, 0, 0, 0., 0., 0, -math.Pow(radius, 2))

	return &QuadSphere{
		Quad:     cylQuad,
		Material: material,
	}
}

func (c *QuadSphere) Clone() Object {
	return &QuadSphere{
		Quad:     c.Quad,
		Color:    c.Color,
		Material: c.Material,
	}
}

// takes inverse tranformation matrix as argument
func (c *QuadSphere) applyMatrix(transformMatrix matrix.Mat) {
	// transpose the inverse translation matrix
	transformMatrixTransp := transformMatrix.Transpose()

	// create Q' = Q' * T^-1
	quadric, _ := transformMatrix.Mult(c.Quad.QuadMatrix)

	// quadric, _ = quadric.Mult(translateInv)

	// create Q' = T^-1^T * Q
	quadric, _ = transformMatrixTransp.Mult(quadric)

	// update quadric matrix and sync
	c.Quad.QuadMatrix = quadric
	c.Quad.sync()

}

func (c *QuadSphere) Scale(scalar float64) {

	// create a translation matrix
	scale, _ := matrix.Identity(4)
	scale.Set(0, 0, 1./scalar)
	scale.Set(1, 1, 1./scalar)
	scale.Set(2, 2, 1./scalar)

	// create the Inverse translation matrix
	scaleInv, _ := scale.Inverse()

	c.applyMatrix(scaleInv)

}

// Shift moves the object by the given coordinates
func (c *QuadSphere) Shift(coords vec.Vec3) {

	// create a translation matrix
	translate, _ := matrix.Identity(4)
	translate.Set(0, 3, coords.X)
	translate.Set(1, 3, coords.Y)
	translate.Set(2, 3, coords.Z)

	// create the Inverse translation matrix
	translateInv, _ := translate.Inverse()

	c.applyMatrix(translateInv)
}

func (c *QuadSphere) ScaleX(x float64) {

	// create a translation matrix
	scale, _ := matrix.Identity(4)
	scale.Set(0, 0, 1./x)

	// create the Inverse translation matrix
	scaleInv, _ := scale.Inverse()

	c.applyMatrix(scaleInv)

}

func (c *QuadSphere) ScaleY(y float64) {

	// create a translation matrix
	scale, _ := matrix.Identity(4)
	scale.Set(1, 1, 1./y)

	// create the Inverse translation matrix
	scaleInv, _ := scale.Inverse()

	c.applyMatrix(scaleInv)

}

func (c *QuadSphere) ScaleZ(z float64) {

	// create a translation matrix
	scale, _ := matrix.Identity(4)
	scale.Set(2, 2, 1./z)

	// create the Inverse translation matrix
	scaleInv, _ := scale.Inverse()

	c.applyMatrix(scaleInv)

}

func (c *QuadSphere) ShiftX(x float64) {
	translateX, _ := matrix.Identity(4)
	translateX.Set(0, 3, x)

	translateInv, _ := translateX.Inverse()

	c.applyMatrix(translateInv)

}

func (c *QuadSphere) ShiftY(y float64) {
	translateY, _ := matrix.Identity(4)
	translateY.Set(1, 3, y)

	translateInv, _ := translateY.Inverse()

	c.applyMatrix(translateInv)

}

func (c *QuadSphere) ShiftZ(z float64) {
	translateZ, _ := matrix.Identity(4)
	translateZ.Set(0, 3, z)

	translateInv, _ := translateZ.Inverse()

	c.applyMatrix(translateInv)

}

func (c *QuadSphere) RotateX(angle float64) {
	rotateX, _ := matrix.Identity(4)
	rotateX.Set(1, 1, math.Cos(angle))
	rotateX.Set(1, 2, math.Sin(angle)*-1.)
	rotateX.Set(2, 1, math.Sin(angle))
	rotateX.Set(2, 2, math.Cos(angle))

	// invert rotation matrix manually
	rotateXInv, _ := rotateX.Inverse()

	c.applyMatrix(rotateXInv)

}

func (c *QuadSphere) RotateY(angle float64) {
	rotateY, _ := matrix.Identity(4)
	rotateY.Set(0, 0, math.Cos(angle))
	rotateY.Set(0, 2, math.Sin(angle))
	rotateY.Set(3, 0, math.Sin(angle)*-1.)
	rotateY.Set(3, 2, math.Cos(angle))

	// invert rotation matrix manually
	rotateYInv, _ := rotateY.Inverse()

	c.applyMatrix(rotateYInv)
}

func (c *QuadSphere) RotateZ(angle float64) {
	rotateZ, _ := matrix.Identity(4)
	rotateZ.Set(0, 0, math.Cos(angle))
	rotateZ.Set(0, 1, math.Sin(angle)*-1.)
	rotateZ.Set(1, 0, math.Sin(angle))
	rotateZ.Set(1, 1, math.Cos(angle))

	// invert rotation matrix manually
	rotateZInv, _ := rotateZ.Inverse()

	c.applyMatrix(rotateZInv)
}

func (c *QuadSphere) Intersect(ray ray.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {

	// compute the quadratic coefficients for the intersection equation
	a := c.Quad.A*ray.Direction.X*ray.Direction.X + c.Quad.B*ray.Direction.Y*ray.Direction.Y + c.Quad.C*ray.Direction.Z*ray.Direction.Z + 2*c.Quad.D*ray.Direction.X*ray.Direction.Y + 2*c.Quad.E*ray.Direction.X*ray.Direction.Z + 2*c.Quad.F*ray.Direction.Y*ray.Direction.Z

	b := 2*c.Quad.A*ray.Direction.X*ray.Origin.X + 2*c.Quad.B*ray.Direction.Y*ray.Origin.Y + 2*c.Quad.C*ray.Direction.Z*ray.Origin.Z + 2*c.Quad.D*ray.Direction.X*ray.Origin.Y + 2*c.Quad.D*ray.Direction.Y*ray.Origin.X + 2*c.Quad.E*ray.Direction.X*ray.Origin.Z + 2*c.Quad.E*ray.Direction.Z*ray.Origin.X + 2*c.Quad.F*ray.Direction.Y*ray.Origin.Z + 2*c.Quad.F*ray.Direction.Z*ray.Origin.Y + 2*c.Quad.G*ray.Direction.X + 2*c.Quad.H*ray.Direction.Y + 2*c.Quad.I*ray.Direction.Z

	c_ := c.Quad.A*ray.Origin.X*ray.Origin.X + c.Quad.B*ray.Origin.Y*ray.Origin.Y + c.Quad.C*ray.Origin.Z*ray.Origin.Z + 2*c.Quad.D*ray.Origin.X*ray.Origin.Y + 2*c.Quad.E*ray.Origin.X*ray.Origin.Z + 2*c.Quad.F*ray.Origin.Y*ray.Origin.Z + 2*c.Quad.G*ray.Origin.X + 2*c.Quad.H*ray.Origin.Y + 2*c.Quad.I*ray.Origin.Z + c.Quad.J

	// compute the discriminant of the quadratic equation
	discriminant := b*b - 4*a*c_

	// fmt.Println("discriminant: ", discriminant)

	// if the discriminant is negative, there are no real solutions, and the ray misses the quadric
	if discriminant < 0 {
		return false, 0, 0, vec.Vec3{}
	}
	// compute the two solutions of the quadratic equation
	t1 := (-b - math.Sqrt(discriminant)) / (2 * a)
	t2 := (-b + math.Sqrt(discriminant)) / (2 * a)

	//fmt.Println("t1: ", t1, " | t2: ", t2)

	// compute the hit distance (the smaller non-negative solution)
	if t1 > 0 && t2 > 0 {
		distance = math.Min(t1, t2)
		out := math.Max(t1, t2)
		return true, distance, out, c.Normal(ray.Position(distance))
	} else if t1 > 0 {
		distance = t1
		return true, distance, 0., c.Normal(ray.Position(distance))
	} else if t2 > 0 {
		distance = t2
		return true, distance, 0, c.Normal(ray.Position(distance))
	} else {
		return false, 0, 0, vec.Vec3{}
	}
}

func (c QuadSphere) GetPosition() vec.Vec3 {
	return vec.Vec3{X: c.Quad.G, Y: c.Quad.H, Z: c.Quad.I}
}

func (c QuadSphere) Albedo() vec.Vec3 {
	return c.Color
}

func (c QuadSphere) GetMaterial() Material {
	return c.Material
}

func (c *QuadSphere) Normal(point vec.Vec3) vec.Vec3 {
	x, y, z := point.X, point.Y, point.Z

	// use derivatives of the quadric equation to compute the normal, slide 69
	nx := 2*c.Quad.A*x + 2*c.Quad.D*y + 2*c.Quad.E*z + 2*c.Quad.G
	ny := 2*c.Quad.B*y + 2*c.Quad.D*x + 2*c.Quad.F*z + 2*c.Quad.H
	nz := 2*c.Quad.C*z + 2*c.Quad.E*x + 2*c.Quad.F*y + 2*c.Quad.I

	// fmt.Println("quadric normal: ", vec.Vec3{X: nx, Y: ny, Z: nz}.Norm())

	return vec.Vec3{X: nx, Y: ny, Z: nz}.Norm()
}

func (c *QuadSphere) AddMaterial(mat Material) {
	c.Material = mat
}

func (c *QuadSphere) PrintQuadParams() {
	// print it as a quadric matrix
	fmt.Println("Quadric Matrix: ")
	fmt.Println(c.Quad.A, c.Quad.D, c.Quad.E, c.Quad.G)
	fmt.Println(c.Quad.D, c.Quad.B, c.Quad.F, c.Quad.H)
	fmt.Println(c.Quad.E, c.Quad.F, c.Quad.C, c.Quad.I)
	fmt.Println(c.Quad.G, c.Quad.H, c.Quad.I, c.Quad.J)
}
