package objects

import (
	"ray/camera"
	vec "ray/vector"
)

// constructive solid geometry

// creates a new object by using boolean operations on two objects with quadrics as their base

// a few new structs for csg objects

// **** UNITY ****
type Unity struct {
	ObjectA  Object
	ObjectB  Object
	Material Material
}

func NewUnity(objectA Object, objectB Object, color vec.Vec3) *Unity {
	material := Material{ // default material
		Color:     color,
		Roughness: .1,
		Metalness: .9,
		Diffuse:   .9,
	}
	A := objectA
	B := objectB
	return &Unity{
		ObjectA:  A,
		ObjectB:  B,
		Material: material,
	}
}

func (u *Unity) Clone() Object {
	return &Intersection{
		ObjectA:  u.ObjectA.Clone(),
		ObjectB:  u.ObjectB.Clone(),
		Material: u.Material,
	}
}

func (u *Unity) Intersect(ray camera.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {
	// we are going to return the closest intersection
	// if there is no intersection, we return false, 0
	hitA, intPointA, outA, normalA := u.ObjectA.Intersect(ray)
	hitB, intPointB, outB, normalB := u.ObjectB.Intersect(ray)
	if hitA && hitB {
		if intPointA < intPointB {
			return true, intPointA, outA, normalA
		} else {
			return true, intPointB, outB, normalB
		}
	}
	if hitA {
		return true, intPointA, outA, normalA
	}
	if hitB {
		return true, intPointB, outB, normalB
	}
	return false, 0, 0, vec.Vec3{X: 0, Y: 0, Z: 0}
}

func (u *Unity) Scale(float64) {

}

func (u *Unity) ScaleX(float64) {

}
func (u *Unity) ScaleY(float64) {

}
func (u *Unity) ScaleZ(float64) {

}
func (u *Unity) Shift(offset vec.Vec3) {
	u.ObjectA.Shift(offset)
	u.ObjectB.Shift(offset)

}
func (u *Unity) ShiftX(float64) {

}
func (u *Unity) ShiftY(float64) {

}
func (u *Unity) ShiftZ(float64) {

}
func (u *Unity) RotateX(float64) {

}
func (u *Unity) RotateY(float64) {

}
func (u *Unity) RotateZ(float64) {

}

func (u Unity) GetPosition() vec.Vec3 {
	posA := u.ObjectA.GetPosition()
	posB := u.ObjectB.GetPosition()
	return posA.Add(posB).ScalarDiv(2)
}

func (u *Unity) Albedo() vec.Vec3 {
	return vec.Vec3{X: 0, Y: 0, Z: 0}
}
func (u *Unity) Normal(point vec.Vec3) vec.Vec3 {
	// intersect with object A
	_, distA, _, _ := u.ObjectA.Intersect(camera.Ray{Origin: point, Direction: vec.Vec3{}})

	// intersect with object B
	_, distB, _, _ := u.ObjectB.Intersect(camera.Ray{Origin: point, Direction: vec.Vec3{}})

	// determine which object was hit first
	if distA < distB {
		// calculate normal based on objectA
		return u.ObjectA.Normal(point)
	} else {
		// calculate normal based on objectB
		return u.ObjectB.Normal(point)
	}
}

func (u *Unity) GetMaterial() Material {
	return u.Material
}
func (u *Unity) AddMaterial(mat Material) {}

// **** INTERSECTION ****

type Intersection struct {
	ObjectA  Object
	ObjectB  Object
	Name     string
	Material Material
}

func NewIntersection(objectA Object, objectB Object, color vec.Vec3) *Intersection {
	material := Material{ // default material
		Color:     color,
		Roughness: .1,
		Metalness: .9,
		Diffuse:   .9,
	}

	A := objectA
	B := objectB

	return &Intersection{
		ObjectA:  A,
		ObjectB:  B,
		Name:     "intersection",
		Material: material,
	}
}

func (i *Intersection) Clone() Object {
	return &Intersection{
		ObjectA:  i.ObjectA.Clone(),
		ObjectB:  i.ObjectB.Clone(),
		Material: i.Material,
	}
}

func (i Intersection) Intersect(ray camera.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {
	hitA, distA, outA, _ := i.ObjectA.Intersect(ray)
	hitB, distB, outB, _ := i.ObjectB.Intersect(ray)

	if hitA && hitB {
		if distB < distA {
			//_, _, normalA := i.ObjectA.Intersect(ray)
			// calculate normal based on objectA
			position := ray.Position(distB)
			normalB := i.ObjectB.Normal(position)
			return true, distA, outA, normalB
		} else if distB >= distA {
			//_, _, normalB := i.ObjectB.Intersect(ray)
			position := ray.Position(distB)
			normalB := i.ObjectB.Normal(position)
			return true, distB, outB, normalB
		}
	}

	return false, 0, 0, vec.Vec3{}
}

func (i *Intersection) Scale(factor float64) {
	i.ObjectA.Scale(factor)
	i.ObjectB.Scale(factor)
}

func (i *Intersection) ScaleX(factor float64) {
	i.ObjectA.ScaleX(factor)
	i.ObjectB.ScaleX(factor)
}

func (i *Intersection) ScaleY(factor float64) {
	i.ObjectA.ScaleY(factor)
	i.ObjectB.ScaleY(factor)
}

func (i *Intersection) ScaleZ(factor float64) {
	i.ObjectA.ScaleZ(factor)
	i.ObjectB.ScaleZ(factor)
}

func (i *Intersection) Shift(offset vec.Vec3) {
	i.ObjectA.Shift(offset)
	i.ObjectB.Shift(offset)
}

func (i *Intersection) ShiftX(offset float64) {
	i.ObjectA.ShiftX(offset)
	i.ObjectB.ShiftX(offset)
}

func (i *Intersection) ShiftY(offset float64) {
	i.ObjectA.ShiftY(offset)
	i.ObjectB.ShiftY(offset)
}

func (i *Intersection) ShiftZ(offset float64) {
	i.ObjectA.ShiftZ(offset)
	i.ObjectB.ShiftZ(offset)
}

func (i *Intersection) RotateX(angle float64) {
	i.ObjectA.RotateX(angle)
	i.ObjectB.RotateX(angle)
}

func (i *Intersection) RotateY(angle float64) {
	i.ObjectA.RotateY(angle)
	i.ObjectB.RotateY(angle)
}

func (i *Intersection) RotateZ(angle float64) {
	i.ObjectA.RotateZ(angle)
	i.ObjectB.RotateZ(angle)
}

func (i Intersection) GetPosition() vec.Vec3 {
	posA := i.ObjectA.GetPosition()
	posB := i.ObjectB.GetPosition()
	return posA.Add(posB).ScalarDiv(2)
}

func (i *Intersection) Albedo() vec.Vec3 {
	return vec.Vec3{}
}

/* func (i *Intersection) Normal(point vec.Vec3) vec.Vec3 {
	_, _, _, normal := i.Intersect(camera.Ray{}) // Assuming the ray parameter is not relevant for normal calculation
	return normal
} */

func (i *Intersection) Normal(point vec.Vec3) vec.Vec3 {
	// intersect with object A
	_, distA, _, _ := i.ObjectA.Intersect(camera.Ray{Origin: point, Direction: vec.Vec3{}})

	// intersect with object B
	_, distB, _, _ := i.ObjectB.Intersect(camera.Ray{Origin: point, Direction: vec.Vec3{}})

	// determine which object was hit first
	if distA < distB {
		// calculate normal based on objectA
		return i.ObjectA.Normal(point)
	} else {
		// calculate normal based on objectB
		return i.ObjectB.Normal(point)
	}
}

func (i *Intersection) GetMaterial() Material {
	return i.Material
}

func (i *Intersection) AddMaterial(mat Material) {
	// Ignore adding material in intersection
	i.Material = mat
}

// **** DIFFERENCE ****

type Difference struct {
	ObjectA  Object
	ObjectB  Object
	Material Material
}

func NewDifference(objectA Object, objectB Object, color vec.Vec3) *Difference {
	material := Material{ // default material
		Color:     color,
		Roughness: .1,
		Metalness: .9,
		Diffuse:   .9,
	}
	A := objectA
	B := objectB
	return &Difference{
		ObjectA:  A,
		ObjectB:  B,
		Material: material,
	}
}

func (d *Difference) Clone() Object {
	return &Difference{
		ObjectA:  d.ObjectA.Clone(),
		ObjectB:  d.ObjectB.Clone(),
		Material: d.Material,
	}
}

func (d *Difference) Intersect(ray camera.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {
	// Calculate intersection with object A
	hitA, A_in, A_out, _ := d.ObjectA.Intersect(ray)

	// Calculate intersection with object B
	hitB, B_in, B_out, _ := d.ObjectB.Intersect(ray)

	// If ray hits object A but not object B, return intersection with object A
	if hitA && !hitB {
		return true, A_in, A_out, d.ObjectA.Normal(ray.Position(A_in))
	}

	// If ray hits object B but not object A, return nothing
	if hitB && !hitA {
		return false, 0, 0, vec.Vec3{}
	}

	// if ray hits both objects, check if object B is hit first
	if hitA && hitB {

		// if A_in is nearer than B_in, return intersection with object A
		if A_in < B_in {
			return true, A_in, A_out, d.ObjectA.Normal(ray.Position(A_in))
		}

		// if B_in is nearer than A_in we need to check if the ray intersects with object A
		if B_in < A_in && B_out < A_out {
			return true, B_out, A_out, d.ObjectA.Normal(ray.Position(B_out))
		} else if B_in < A_in && B_out < A_in {
			return true, A_in, A_out, d.ObjectA.Normal(ray.Position(A_in))
		}

	}
	return false, 0, 0, vec.Vec3{}

}

func (d *Difference) Scale(factor float64) {
	d.ObjectA.Scale(factor)
	d.ObjectB.Scale(factor)
}

func (d *Difference) ScaleX(factor float64) {
	d.ObjectA.ScaleX(factor)
	d.ObjectB.ScaleX(factor)
}

func (d *Difference) ScaleY(factor float64) {
	d.ObjectA.ScaleY(factor)
	d.ObjectB.ScaleY(factor)
}

func (d *Difference) ScaleZ(factor float64) {
	d.ObjectA.ScaleZ(factor)
	d.ObjectB.ScaleZ(factor)
}

func (d *Difference) Shift(offset vec.Vec3) {
	d.ObjectA.Shift(offset)
	d.ObjectB.Shift(offset)
}

func (d *Difference) ShiftX(offset float64) {
	d.ObjectA.ShiftX(offset)
	d.ObjectB.ShiftX(offset)
}

func (d *Difference) ShiftY(offset float64) {
	d.ObjectA.ShiftY(offset)
	d.ObjectB.ShiftY(offset)
}

func (d *Difference) ShiftZ(offset float64) {
	d.ObjectA.ShiftZ(offset)
	d.ObjectB.ShiftZ(offset)
}

func (d *Difference) RotateX(angle float64) {
	d.ObjectA.RotateX(angle)
	d.ObjectB.RotateX(angle)
}

func (d *Difference) RotateY(angle float64) {
	d.ObjectA.RotateY(angle)
	d.ObjectB.RotateY(angle)
}

func (d *Difference) RotateZ(angle float64) {
	d.ObjectA.RotateZ(angle)
	d.ObjectB.RotateZ(angle)
}

func (d Difference) GetPosition() vec.Vec3 {
	posA := d.ObjectA.GetPosition()
	posB := d.ObjectB.GetPosition()
	return posA.Add(posB).ScalarDiv(2)
}

func (d *Difference) Albedo() vec.Vec3 {
	return vec.Vec3{}
}

func (d *Difference) Normal(point vec.Vec3) vec.Vec3 {
	_, _, _, normal := d.Intersect(camera.Ray{}) // Assuming the ray parameter is not relevant for normal calculation
	return normal
}

func (d *Difference) GetMaterial() Material {
	return d.Material
}

func (d *Difference) AddMaterial(mat Material) {
	// Ignore adding material in intersection
	d.Material = mat
}
