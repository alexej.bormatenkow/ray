package objects

// quadric is used as a struct for komplex objects
// every complex object defines it's own ray intersection and should be able to
// be used in constructive solid geometry

import (
	"ray/matrix"
)

type Quadric struct {
	A, B, C, D, E, F, G, H, I, J float64
	QuadMatrix                   matrix.Mat
}

func NewQuadric(a, b, c, d, e, f, g, h, i, j float64) Quadric {
	quadMatrix, _ := matrix.NewMatrix(4)

	quadMatrix.Set(0, 0, a)
	quadMatrix.Set(0, 1, d)
	quadMatrix.Set(0, 2, e)
	quadMatrix.Set(0, 3, g)

	quadMatrix.Set(1, 0, d)
	quadMatrix.Set(1, 1, b)
	quadMatrix.Set(1, 2, f)
	quadMatrix.Set(1, 3, h)

	quadMatrix.Set(2, 0, e)
	quadMatrix.Set(2, 1, f)
	quadMatrix.Set(2, 2, c)
	quadMatrix.Set(2, 3, i)

	quadMatrix.Set(3, 0, g)
	quadMatrix.Set(3, 1, h)
	quadMatrix.Set(3, 2, i)
	quadMatrix.Set(3, 3, j)

	return Quadric{
		A:          a,
		B:          b,
		C:          c,
		D:          d,
		E:          e,
		F:          f,
		G:          g,
		H:          h,
		I:          i,
		J:          j,
		QuadMatrix: *quadMatrix,
	}
}

// keep parameters and internal matrix in sync
func (q *Quadric) sync() {
	q.A = q.QuadMatrix.Get(0, 0)
	q.B = q.QuadMatrix.Get(1, 1)
	q.C = q.QuadMatrix.Get(2, 2)
	q.D = q.QuadMatrix.Get(0, 1)
	q.E = q.QuadMatrix.Get(0, 2)
	q.F = q.QuadMatrix.Get(1, 2)
	q.G = q.QuadMatrix.Get(0, 3)
	q.H = q.QuadMatrix.Get(3, 1)
	q.I = q.QuadMatrix.Get(2, 3)
	q.J = q.QuadMatrix.Get(3, 3)
}
