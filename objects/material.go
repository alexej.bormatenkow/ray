package objects

import (
	vec "ray/vector"
)

type Material struct {
	Color        vec.Vec3
	Roughness    float64
	Metalness    float64
	Reflectivity float64
	Transmission float64
	Diffuse      float64
}
