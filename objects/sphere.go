package objects

import (
	"math"
	ray "ray/camera"
	vec "ray/vector"
)

type Sphere struct {
	Center   vec.Vec3
	Radius   float64
	Color    vec.Vec3
	Material Material
}

func NewSphere(coords vec.Vec3, radius float64, color vec.Vec3) *Sphere {
	material := Material{ // default material
		Color:        color,
		Roughness:    .5,
		Metalness:    .3,
		Diffuse:      .7,
		Transmission: .0,
		Reflectivity: .4,
	}
	return &Sphere{
		Center:   coords,
		Radius:   radius,
		Color:    material.Color,
		Material: material,
	}
}

func (s *Sphere) Clone() Object {
	return &Sphere{
		Center:   s.Center,
		Radius:   s.Radius,
		Color:    s.Color,
		Material: s.Material,
	}
}

func (s *Sphere) Scale(radius float64) {
	s.Radius *= radius
}

func (s *Sphere) Shift(coords vec.Vec3) {
	s.Center = s.Center.Add(coords)
}

func (s *Sphere) ScaleX(x float64) {
	s.Center.X *= x
	s.Radius *= x
}

func (s *Sphere) ScaleY(y float64) {
	s.Center.Y *= y
	s.Radius *= y
}

func (s *Sphere) ScaleZ(z float64) {
	s.Center.Z *= z
	s.Radius *= z
}

func (s *Sphere) ShiftX(x float64) {
	s.Center.X += x
}

func (s *Sphere) ShiftY(y float64) {
	s.Center.Y += y
}

func (s *Sphere) ShiftZ(z float64) {
	s.Center.Z += z
}

func (s *Sphere) RotateX(x float64) {
	s.Center.X *= x
	s.Radius *= x
}

func (s *Sphere) RotateY(y float64) {
	s.Center.Y *= y
	s.Radius *= y
}

func (s *Sphere) RotateZ(z float64) {
	s.Center.Z *= z
	s.Radius *= z
}

func (s *Sphere) Intersect(ray ray.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {

	// "The Ray Tracer Challenge", p. 61 ff
	sphere_to_ray := ray.Origin.Sub(s.Center)
	a := ray.Direction.Dot(ray.Direction)
	b := 2 * ray.Direction.Dot(sphere_to_ray)
	c := sphere_to_ray.Dot(sphere_to_ray) - s.Radius*s.Radius

	discriminant := b*b - 4*a*c

	// fmt.Printf("discriminant %v \n", discriminant)
	if discriminant < 0. {
		return false, 0, 0, vec.Vec3{X: 0, Y: 0, Z: 0}
	}

	// citardauq
	t1 := (-b - math.Sqrt(discriminant)) / (2 * a)
	t2 := (-b + math.Sqrt(discriminant)) / (2 * a)

	// fmt.Printf("t1: %v, t2: %v \n", t1, t2)

	// implementation of a hit:
	// spheres can have 2 intersections
	// we only need the intersection near to the ray origin, for csg we export both
	// we also discard every intersection which is negative / behind the ray origin

	if (t1 > 0) && t1 < t2 {
		return true, t1, t2, s.Normal(ray.Position(t1))
	} else if (t2 > 0) && (t2 < t1) {
		return true, t2, t1, s.Normal(ray.Position(t2))
	} else {
		return false, 0, 0, vec.Vec3{X: 0, Y: 0, Z: 0}
	}

}

func (s Sphere) GetPosition() vec.Vec3 {
	return s.Center
}

func (s *Sphere) Albedo() vec.Vec3 {
	return s.Color
}

func (s *Sphere) GetMaterial() Material {
	return s.Material
}

func (s *Sphere) Normal(point vec.Vec3) vec.Vec3 {
	return point.Sub(s.Center).Norm()
	// return point.Sub(s.Center).ScalarDiv(s.Radius)
}

func (s *Sphere) AddMaterial(mat Material) {
	s.Material = mat
}
