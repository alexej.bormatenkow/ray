package objects

import (
	"math"
	ray "ray/camera"
	"ray/matrix"
	vec "ray/vector"
)

type QuadObject struct {
	Quad     Quadric
	Color    vec.Vec3
	Material Material
}

func NewQuadObject(A, B, C, D, E, F, G, H, I, J float64, color vec.Vec3) *QuadObject {
	material := Material{ // default material
		Color:     color,
		Roughness: .02,
		Metalness: .8,
		Diffuse:   .8,
	}

	objQuad := NewQuadric(A, B, C, D, E, F, G, H, I, J)

	return &QuadObject{
		Quad:     objQuad,
		Material: material,
	}
}

func (q *QuadObject) Clone() Object {
	return &Cylinder{
		Quad:     q.Quad,
		Color:    q.Color,
		Material: q.Material,
	}
}

// takes inverse tranformation matrix as argument
func (c *QuadObject) applyMatrix(transformMatrix matrix.Mat) {
	// transpose the inverse translation matrix
	transformMatrixTransp := transformMatrix.Transpose()

	// create Q' = Q' * T^-1
	quadric, _ := transformMatrix.Mult(c.Quad.QuadMatrix)

	// quadric, _ = quadric.Mult(translateInv)

	// create Q' = T^-1^T * Q
	quadric, _ = transformMatrixTransp.Mult(quadric)

	// update quadric matrix and sync
	c.Quad.QuadMatrix = quadric
	c.Quad.sync()

}

// scale and shift operations should be implemented with matrix transformations

func (q *QuadObject) Scale(scalar float64) {
	scale, _ := matrix.Identity(4)
	scale.Set(0, 0, scalar) // x
	scale.Set(1, 1, scalar) // y
	scale.Set(2, 2, scalar) // z
}

// Shift moves the object by the given coordinates
func (q *QuadObject) Shift(coords vec.Vec3) {

	// create a translation matrix
	translate, _ := matrix.Identity(4)
	translate.Set(0, 3, coords.X)
	translate.Set(1, 3, coords.Y)
	translate.Set(2, 3, coords.Z)

	// create the Inverse translation matrix
	translateInv, _ := translate.Inverse()

	q.applyMatrix(translateInv)
}

func (q *QuadObject) ScaleX(x float64) {

}

func (q *QuadObject) ScaleY(y float64) {

}

func (q *QuadObject) ScaleZ(z float64) {

}

func (q *QuadObject) ShiftX(x float64) {
	translateX, _ := matrix.Identity(4)
	translateX.Set(0, 3, x)

}

func (q *QuadObject) ShiftY(y float64) {
	translateY, _ := matrix.Identity(4)
	translateY.Set(0, 3, y)

}

func (q *QuadObject) ShiftZ(z float64) {
	translateZ, _ := matrix.Identity(4)
	translateZ.Set(0, 3, z)

}

func (q *QuadObject) RotateX(angle float64) {
	rotateX, _ := matrix.Identity(4)
	rotateX.Set(1, 1, math.Cos(angle))
	rotateX.Set(1, 2, math.Sin(angle)*-1.)
	rotateX.Set(2, 1, math.Sin(angle))
	rotateX.Set(2, 2, math.Cos(angle))
}

func (q *QuadObject) RotateY(angle float64) {
	rotateY, _ := matrix.Identity(4)
	rotateY.Set(0, 0, math.Cos(angle))
	rotateY.Set(0, 2, math.Sin(angle))
	rotateY.Set(3, 0, math.Sin(angle)*-1.)
	rotateY.Set(3, 2, math.Cos(angle))
}

func (q *QuadObject) RotateZ(angle float64) {
	rotateZ, _ := matrix.Identity(4)
	rotateZ.Set(0, 0, math.Cos(angle))
	rotateZ.Set(0, 1, math.Sin(angle)*-1.)
	rotateZ.Set(1, 0, math.Sin(angle))
	rotateZ.Set(1, 1, math.Cos(angle))
}

func (q *QuadObject) Intersect(ray ray.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {

	// compute the quadratic coefficients for the intersection equation
	a := q.Quad.A*ray.Direction.X*ray.Direction.X + q.Quad.B*ray.Direction.Y*ray.Direction.Y + q.Quad.C*ray.Direction.Z*ray.Direction.Z + 2*q.Quad.D*ray.Direction.X*ray.Direction.Y + 2*q.Quad.E*ray.Direction.X*ray.Direction.Z + 2*q.Quad.F*ray.Direction.Y*ray.Direction.Z

	b := 2*q.Quad.A*ray.Direction.X*ray.Origin.X + 2*q.Quad.B*ray.Direction.Y*ray.Origin.Y + 2*q.Quad.C*ray.Direction.Z*ray.Origin.Z + 2*q.Quad.D*ray.Direction.X*ray.Origin.Y + 2*q.Quad.D*ray.Direction.Y*ray.Origin.X + 2*q.Quad.E*ray.Direction.X*ray.Origin.Z + 2*q.Quad.E*ray.Direction.Z*ray.Origin.X + 2*q.Quad.F*ray.Direction.Y*ray.Origin.Z + 2*q.Quad.F*ray.Direction.Z*ray.Origin.Y + 2*q.Quad.G*ray.Direction.X + 2*q.Quad.H*ray.Direction.Y + 2*q.Quad.I*ray.Direction.Z

	c_ := q.Quad.A*ray.Origin.X*ray.Origin.X + q.Quad.B*ray.Origin.Y*ray.Origin.Y + q.Quad.C*ray.Origin.Z*ray.Origin.Z + 2*q.Quad.D*ray.Origin.X*ray.Origin.Y + 2*q.Quad.E*ray.Origin.X*ray.Origin.Z + 2*q.Quad.F*ray.Origin.Y*ray.Origin.Z + 2*q.Quad.G*ray.Origin.X + 2*q.Quad.H*ray.Origin.Y + 2*q.Quad.I*ray.Origin.Z + q.Quad.J

	// compute the discriminant of the quadratic equation
	discriminant := b*b - 4*a*c_

	// fmt.Println("discriminant: ", discriminant)

	// if the discriminant is negative, there are no real solutions, and the ray misses the quadric
	if discriminant < 0 {
		return false, 0, 0, vec.Vec3{}
	}
	// compute the two solutions of the quadratic equation
	t1 := (-b - math.Sqrt(discriminant)) / (2 * a)
	t2 := (-b + math.Sqrt(discriminant)) / (2 * a)

	//fmt.Println("t1: ", t1, " | t2: ", t2)

	// compute the hit distance (the smaller non-negative solution)
	if t1 > 0 && t2 > 0 {
		distance = math.Min(t1, t2)
		out = math.Max(t1, t2)
		return true, distance, out, q.Normal(ray.Position(distance))
	} else if t1 > 0 {
		distance = t1
		return true, distance, 0, q.Normal(ray.Position(distance))
	} else if t2 > 0 {
		distance = t2
		return true, distance, 0, q.Normal(ray.Position(distance))
	} else {
		return false, 0, 0, vec.Vec3{}
	}
}

func (c QuadObject) GetPosition() vec.Vec3 {
	return vec.Vec3{X: c.Quad.G, Y: c.Quad.H, Z: c.Quad.I}
}

func (q QuadObject) Albedo() vec.Vec3 {
	return q.Color
}

func (q QuadObject) GetMaterial() Material {
	return q.Material
}

func (q *QuadObject) Normal(point vec.Vec3) vec.Vec3 {
	x, y, z := point.X, point.Y, point.Z

	// use derivatives of the quadric equation to compute the normal, slide 69
	nx := q.Quad.A*x + q.Quad.D*y + q.Quad.E*z + q.Quad.G
	ny := q.Quad.B*y + q.Quad.D*x + q.Quad.F*z + q.Quad.H
	nz := q.Quad.C*z + q.Quad.E*x + q.Quad.F*y + q.Quad.I

	// fmt.Println("quadric normal: ", vec.Vec3{X: nx, Y: ny, Z: nz}.Norm())

	return vec.Vec3{X: nx, Y: ny, Z: nz}.Norm()
}

func (q *QuadObject) AddMaterial(mat Material) {
	q.Material = mat
}
