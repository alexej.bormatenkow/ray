package objects

import (
	cam "ray/camera"
	vec "ray/vector"
)

type Object interface {
	Scale(float64)
	ScaleX(float64)
	ScaleY(float64)
	ScaleZ(float64)
	Shift(vec.Vec3)
	ShiftX(float64)
	ShiftY(float64)
	ShiftZ(float64)
	RotateX(float64)
	RotateY(float64)
	RotateZ(float64)
	Intersect(ray cam.Ray) (hit bool, distance float64, out float64, normal vec.Vec3)
	GetPosition() vec.Vec3
	Albedo() vec.Vec3
	Normal(point vec.Vec3) vec.Vec3
	GetMaterial() Material
	AddMaterial(mat Material)
	Clone() Object
}
