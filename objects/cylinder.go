package objects

import (
	"math"
	ray "ray/camera"
	"ray/matrix"
	vec "ray/vector"
)

type Cylinder struct {
	Quad     Quadric
	Color    vec.Vec3
	Material Material
}

func NewCylinder(radius float64, color vec.Vec3) *Cylinder {
	material := Material{ // default material
		Color:     color,
		Roughness: .02,
		Metalness: .8,
		Diffuse:   .8,
	}

	cylQuad := NewQuadric(0., 1., 1., 0, 0, 0, 0, 0, 0, -math.Pow(radius, 2))

	return &Cylinder{
		Quad:     cylQuad,
		Material: material,
	}
}

func (c *Cylinder) Clone() Object {
	return &Cylinder{
		Quad:     c.Quad,
		Color:    c.Color,
		Material: c.Material,
	}
}

// scale and shift operations should be implemented with matrix transformations

func (c *Cylinder) Scale(scalar float64) {
	scale, _ := matrix.Identity(4)
	scale.Set(0, 0, scalar) // x
	scale.Set(1, 1, scalar) // y
	scale.Set(2, 2, scalar) // z
}

// Shift moves the object by the given coordinates
func (c *Cylinder) Shift(coords vec.Vec3) {

	// create a translation matrix
	translate, _ := matrix.Identity(4)
	translate.Set(0, 3, coords.X)
	translate.Set(1, 3, coords.Y)
	translate.Set(2, 3, coords.Z)

	// create the Inverse translation matrix
	translateInv, _ := matrix.Identity(4)
	translateInv.Set(0, 3, -coords.X)
	translateInv.Set(1, 3, -coords.Y)
	translateInv.Set(2, 3, -coords.Z)

	// transpose the inverse translation matrix
	translateInvTransp := translateInv.Transpose()

	// create Q' = T^-1^T * Q
	quadric, _ := translateInvTransp.Mult(c.Quad.QuadMatrix)

	// create Q' = Q' * T^-1
	quadric, _ = quadric.Mult(translateInv)

	// update quadric matrix and sync
	c.Quad.QuadMatrix = quadric
	c.Quad.sync()

}

func (c *Cylinder) ScaleX(x float64) {

}

func (c *Cylinder) ScaleY(y float64) {

}

func (c *Cylinder) ScaleZ(z float64) {

}

func (c *Cylinder) ShiftX(x float64) {
	translateX, _ := matrix.Identity(4)
	translateX.Set(0, 3, x)

}

func (c *Cylinder) ShiftY(y float64) {
	translateY, _ := matrix.Identity(4)
	translateY.Set(0, 3, y)

}

func (c *Cylinder) ShiftZ(z float64) {
	translateZ, _ := matrix.Identity(4)
	translateZ.Set(0, 3, z)

}

func (c *Cylinder) RotateX(angle float64) {
	rotateX, _ := matrix.Identity(4)
	rotateX.Set(1, 1, math.Cos(angle))
	rotateX.Set(1, 2, math.Sin(angle)*-1.)
	rotateX.Set(2, 1, math.Sin(angle))
	rotateX.Set(2, 2, math.Cos(angle))
}

func (c *Cylinder) RotateY(angle float64) {
	rotateY, _ := matrix.Identity(4)
	rotateY.Set(0, 0, math.Cos(angle))
	rotateY.Set(0, 2, math.Sin(angle))
	rotateY.Set(3, 0, math.Sin(angle)*-1.)
	rotateY.Set(3, 2, math.Cos(angle))
}

func (c *Cylinder) RotateZ(angle float64) {
	rotateZ, _ := matrix.Identity(4)
	rotateZ.Set(0, 0, math.Cos(angle))
	rotateZ.Set(0, 1, math.Sin(angle)*-1.)
	rotateZ.Set(1, 0, math.Sin(angle))
	rotateZ.Set(1, 1, math.Cos(angle))
}

func (c *Cylinder) Intersect(ray ray.Ray) (hit bool, distance float64, out float64, normal vec.Vec3) {

	// compute the quadratic coefficients for the intersection equation
	a := c.Quad.A*ray.Direction.X*ray.Direction.X + c.Quad.B*ray.Direction.Y*ray.Direction.Y + c.Quad.C*ray.Direction.Z*ray.Direction.Z + 2*c.Quad.D*ray.Direction.X*ray.Direction.Y + 2*c.Quad.E*ray.Direction.X*ray.Direction.Z + 2*c.Quad.F*ray.Direction.Y*ray.Direction.Z

	b := 2*c.Quad.A*ray.Direction.X*ray.Origin.X + 2*c.Quad.B*ray.Direction.Y*ray.Origin.Y + 2*c.Quad.C*ray.Direction.Z*ray.Origin.Z + 2*c.Quad.D*ray.Direction.X*ray.Origin.Y + 2*c.Quad.D*ray.Direction.Y*ray.Origin.X + 2*c.Quad.E*ray.Direction.X*ray.Origin.Z + 2*c.Quad.E*ray.Direction.Z*ray.Origin.X + 2*c.Quad.F*ray.Direction.Y*ray.Origin.Z + 2*c.Quad.F*ray.Direction.Z*ray.Origin.Y + 2*c.Quad.G*ray.Direction.X + 2*c.Quad.H*ray.Direction.Y + 2*c.Quad.I*ray.Direction.Z

	c_ := c.Quad.A*ray.Origin.X*ray.Origin.X + c.Quad.B*ray.Origin.Y*ray.Origin.Y + c.Quad.C*ray.Origin.Z*ray.Origin.Z + 2*c.Quad.D*ray.Origin.X*ray.Origin.Y + 2*c.Quad.E*ray.Origin.X*ray.Origin.Z + 2*c.Quad.F*ray.Origin.Y*ray.Origin.Z + 2*c.Quad.G*ray.Origin.X + 2*c.Quad.H*ray.Origin.Y + 2*c.Quad.I*ray.Origin.Z + c.Quad.J

	// compute the discriminant of the quadratic equation
	discriminant := b*b - 4*a*c_

	// fmt.Println("discriminant: ", discriminant)

	// if the discriminant is negative, there are no real solutions, and the ray misses the quadric
	if discriminant < 0 {
		return false, 0, 0, vec.Vec3{}
	}
	// compute the two solutions of the quadratic equation
	t1 := (-b - math.Sqrt(discriminant)) / (2 * a)
	t2 := (-b + math.Sqrt(discriminant)) / (2 * a)

	//fmt.Println("t1: ", t1, " | t2: ", t2)

	// compute the hit distance (the smaller non-negative solution)
	if t1 > 0 && t2 > 0 {
		distance = math.Min(t1, t2)
		out = math.Max(t1, t2)
		return true, distance, out, c.Normal(ray.Position(distance))
	} else if t1 > 0 {
		distance = t1
		return true, distance, 0, c.Normal(ray.Position(distance))
	} else if t2 > 0 {
		distance = t2
		return true, distance, 0, c.Normal(ray.Position(distance))
	} else {
		return false, 0, 0, vec.Vec3{}
	}
}

func (c Cylinder) GetPosition() vec.Vec3 {
	return vec.Vec3{X: c.Quad.G, Y: c.Quad.H, Z: c.Quad.I}
}

func (c Cylinder) Albedo() vec.Vec3 {
	return c.Color
}

func (c Cylinder) GetMaterial() Material {
	return c.Material
}

func (c *Cylinder) Normal(point vec.Vec3) vec.Vec3 {
	x, y, z := point.X, point.Y, point.Z

	// use derivatives of the quadric equation to compute the normal, slide 69
	nx := c.Quad.A*x + c.Quad.D*y + c.Quad.E*z + c.Quad.G
	ny := c.Quad.B*y + c.Quad.D*x + c.Quad.F*z + c.Quad.H
	nz := c.Quad.C*z + c.Quad.E*x + c.Quad.F*y + c.Quad.I

	// fmt.Println("quadric normal: ", vec.Vec3{X: nx, Y: ny, Z: nz}.Norm())

	return vec.Vec3{X: nx, Y: ny, Z: nz}.Norm()
}

func (c *Cylinder) AddMaterial(mat Material) {
	c.Material = mat
}
